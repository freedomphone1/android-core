package com.clearos.dlt;

/**
 * Represents a generic response from a remote API where a code range means success and anything else
 * means failure.
 */
public class SuccessResponse {
    private boolean success;
    private int rangeStart = 200;
    private int rangeEnd = 300;

    public SuccessResponse() {}

    /**
     * Set the range of successful HTTP codes.
     * @param rangeStart Start of range *inclusive*.
     * @param rangeEnd End of range *exclusive*.
     */
    public SuccessResponse(int rangeStart, int rangeEnd) {
        this.rangeStart = rangeStart;
        this.rangeEnd = rangeEnd;
    }

    public int getRangeEnd() {
        return rangeEnd;
    }

    public int getRangeStart() {
        return rangeStart;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
    void setCodeInRange(int code) {
        success = code >= rangeStart && code < rangeEnd;
    }
    public boolean isSuccess() {
        return success;
    }
}
