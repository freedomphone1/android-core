package com.clearos.dlt;

import android.util.Log;

import com.goterl.lazysodium.exceptions.SodiumException;

import java.io.IOException;
import java.util.function.Function;

import okhttp3.Response;
import okhttp3.ResponseBody;

public class DidAuthEncryptedEnvelope<T> extends DidAuthApiCallback<T> {
    private static final String TAG = "DidAuthEncryptedEnvelope";
    private DidAuthEnvelope envelope;
    private final DidKeys appKeys;

    public DidAuthEncryptedEnvelope(DidKeys appKeys, Function<T, Void> _callback, String _TAG, Class<T> _typeParameterClass, DidAuthApi _client) {
        super(_callback, _TAG, _typeParameterClass, _client);
        this.appKeys = appKeys;
    }


    public String decrypt() {
        try {
            return CryptoKey.anonDecrypt(envelope.getStatus(), appKeys.getKeys());
        } catch (SodiumException e) {
            Log.e(TAG, "Error decrypting didauth status payload.");
            return null;
        }
    }

    /**
     * Validates the signature for `message` using the server application keys for `packageName`.
     * @return True if the signature is valid.
     */
    public boolean validateSignature() {
        return CryptoKey.verifyHexSignature(envelope.getSignature(), envelope.getMessage(),
                appKeys.getKeys().getPublicKey());
    }


    @Override
    public void onResponse(String source, Response response) {
        try {
            ResponseBody body = response.body();
            String bodyString = body == null ? null : body.string();
            deserializeEnvelope(bodyString);

            if (envelope != null) {
                if (!validateSignature()) {
                    Log.w(TAG, "Couldn't validate signature using provided app keys.");
                    getCallback().apply(null);
                    return;
                }

                String decrypted = decrypt();
                if (decrypted != null) {
                    // This will fire the callback no matter what happens.
                    Log.d(TAG, "Deserializing the decrypted payload: " + decrypted);
                    deserializeResponse(decrypted, response.code());
                } else {
                    Log.w(TAG, "Couldn't decrypt/serialize status payload using provided app keys.");
                    getCallback().apply(null);
                }
            }
        } catch (IOException e) {
            Log.e(TAG, "IO error reading response body.", e);
            getCallback().apply(null);
        }
    }

    private void deserializeEnvelope(String body) {
        if (body == null) {
            Log.e(TAG, "Empty response body from API call.");
            envelope = null;
            return;
        }

        try {
            envelope = getGson().fromJson(body, DidAuthEnvelope.class);
        } catch (Exception e) {
            Log.e(TAG, "Error deserializing envelope from JSON.", e);
            envelope = null;
        }
    }
}

