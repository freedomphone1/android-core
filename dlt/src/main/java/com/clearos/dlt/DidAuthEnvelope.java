package com.clearos.dlt;

public class DidAuthEnvelope {
    private boolean success;
    private String message;
    private String signature;
    private String status;

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public String getSignature() {
        return signature;
    }

    public String getStatus() {
        return status;
    }
}
