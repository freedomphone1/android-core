package com.clearos.dlt;

public class DidAuthClientApi extends DidAuthApi {
    private final DerivedKeyClient keyClient;

    public DidAuthClientApi(DerivedKeyClient keyClient) {
        super(keyClient.getHomeServer());
        this.keyClient = keyClient;
    }

    public DerivedKeyClient getKeyClient() {
        return keyClient;
    }

    /**
     * Creates a new random DID that can be used to identify family members, circles, devices, etc.
     *
     * @param context A string identifying the context in which to derive a deterministic key.
     * @param keyId   An integer specifying the key number (if rotation is being used, increment this number). Or just use 0.
     */
    public DidKeys newDid(String context, long keyId) {
        return CryptoKey.toDidKeys(keyClient.deriveKeyPair(context, keyId));
    }

    /**
     * Creates a new random DID that can be used to identify family members, circles, devices, etc.
     *
     * @param packageName Name of the package to derived keys under. Inferred from context if not
     *                    specified. Must match the calling package name unless the calling package
     *                    is ClearLIFE.
     * @param context A string identifying the context in which to derive a deterministic key.
     * @param keyId   An integer specifying the key number (if rotation is being used, increment this number). Or just use 0.
     */
    public DidKeys newDid(String packageName, String context, long keyId) {
        return CryptoKey.toDidKeys(keyClient.deriveKeyPair(packageName, context, keyId));
    }
}

