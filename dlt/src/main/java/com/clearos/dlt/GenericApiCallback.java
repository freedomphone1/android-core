package com.clearos.dlt;

public interface GenericApiCallback<T> extends ApiCallback {
    Class<T> getParameterizedClass();
    void runCallback(T result, boolean fromCache);
}
