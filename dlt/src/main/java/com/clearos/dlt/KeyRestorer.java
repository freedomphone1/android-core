package com.clearos.dlt;

import io.github.novacrypto.SecureCharBuffer;

public interface KeyRestorer {
    void onRestoreSuccess(CryptoKey cryptoKey);
    void onRestoreError(Exception e);
    void onGenericRestoreSuccess(DidKeys restoredKeys, String fileName, SecureCharBuffer seed);
}
