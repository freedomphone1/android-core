package com.clearos.dlt;

import android.content.Context;
import android.util.Log;

import com.goterl.lazysodium.exceptions.SodiumException;

import io.github.novacrypto.SecureCharBuffer;

public class KeyDecrypter {
    private Context appContext;
    private KeyRestorer restorer;
    private String fileName;

    public KeyDecrypter(Context context, KeyRestorer keyRestorer, String fileName) {
        appContext = context;
        restorer = keyRestorer;
        this.fileName = fileName;
    }

    public void onKeyDecryptSuccess(SecureCharBuffer seedWord) {
        try {
            if (fileName.equals(CryptoKey.MASTER_SEED_FILE))
                restorer.onRestoreSuccess(new CryptoKey(appContext, seedWord, false));
            else {
                Log.d("KD SEED WORD", seedWord.toStringAble().toString());
                DidKeys keys = CryptoKey.fromSecureSeed(seedWord);
                restorer.onGenericRestoreSuccess(keys, fileName, seedWord);
            }
        } catch (SodiumException e) {
            restorer.onRestoreError(e);
        }
    }

    public void onKeyDecryptFailure(Exception e) {
        restorer.onRestoreError(e);
    }
}
