package com.clearos.dlt;

import android.util.Log;

import com.goterl.lazysodium.LazySodiumAndroid;
import com.goterl.lazysodium.SodiumAndroid;
import com.goterl.lazysodium.exceptions.SodiumException;
import com.goterl.lazysodium.interfaces.Hash;

public class MqttTopic extends NotificationTopicSubscription {
    private static final String TAG = "MqttTopic";

    private final String packageName;

    private static final LazySodiumAndroid lazySodium = new LazySodiumAndroid(new SodiumAndroid());
    private static final Hash.Lazy hashLazy = lazySodium;

    public MqttTopic(String appDid, String topic, String packageName) {
        super(appDid, topic);
        this.packageName = packageName;
    }

    public String getPackageName() {
        return packageName;
    }

    /**
     * Returns the SHA256 hash of the appDid, packageName and topic.
     */
    public String getTopicString(String notificationDid) {
        String topicPlain = String.format("%s/%s/%s", getAppDid(), packageName, getTopic());
        String topicHash = "";
        try {
            Log.d(TAG, String.format("Hashing %s as the topic string.", topicPlain));
            topicHash = hashLazy.cryptoHashSha256(topicPlain);
        } catch (SodiumException e) {
            Log.e(TAG, "Unable to hash the plaintext topic.", e);
        }

        topicHash = String.format("%s/%s", notificationDid, topicHash);
        Log.d(TAG, String.format("Returning %s as the topic hash.", topicHash));
        return topicHash;
    }
}
