package com.clearos.dlt.fido;

import duo.labs.webauthn.models.AuthenticatorGetAssertionOptions;
import okhttp3.Call;

public interface AuthenticationHandler extends ListKeysCallback {
    void onAuthBeginSuccess(AuthenticatorGetAssertionOptions options);
    void onAuthCompleteStarted(Call apiCall);
}
