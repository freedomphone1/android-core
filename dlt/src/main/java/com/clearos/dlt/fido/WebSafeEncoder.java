package com.clearos.dlt.fido;

import java.util.Base64;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WebSafeEncoder {
    /**
     * Converts the byte array to base64 replacing web characters `/` with `_` and `+` with `-`.
     */
    public static String webSafeBase64Encode(byte[] value) {
        String b64 = Base64.getEncoder().encodeToString(value);
        return b64.replace('/', '_').replace('+', '-').replaceAll("=", "");
    }

    /**
     * Converts the String in base64 produced by `webSafeBase64Encode` to byte by undoing replacement
     * of web characters `/` with `_` and `+` with `-`.
     */
    public static byte[] webSafeBase64Decode(String value) {
        // Add back any missing '=' that we removed.
        int padding = value.length() % 4;
        String padded = value + Stream.generate(() -> String.valueOf("=")).limit(padding).collect(Collectors.joining());
        String u64 = padded.replace('_', '/').replace('+', '-');
        return Base64.getDecoder().decode(u64);
    }
}
