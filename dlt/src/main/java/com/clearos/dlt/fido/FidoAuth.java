package com.clearos.dlt.fido;

import android.os.CancellationSignal;
import android.util.Log;

import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.MutableLiveData;

import com.clearos.dlt.DecryptedMqttMessage;
import com.clearos.dlt.DidKeys;

import duo.labs.webauthn.Authenticator;
import duo.labs.webauthn.exceptions.NoBiometricsError;
import duo.labs.webauthn.exceptions.VirgilException;
import duo.labs.webauthn.exceptions.WebAuthnException;
import duo.labs.webauthn.models.AuthenticatorGetAssertionOptions;
import duo.labs.webauthn.models.AuthenticatorMakeCredentialOptions;
import okhttp3.Call;

import com.google.gson.JsonObject;

import java.util.function.Function;

public class FidoAuth implements AuthenticateBeginCallback, RegisterBeginCallback {
    private static final String TAG = "FidoAuth";

    private Authenticator fidoClient;
    private String lastKnownChallenge;
    private final FragmentActivity appContext;
    private final AuthApi api;
    private final MutableLiveData<Boolean> processing = new MutableLiveData<>();
    private DidKeys lastKnownSigningKeys;
    private RegistrationHandler registration;
    private AuthenticationHandler authentication;
    private DecryptedMqttMessage latestMessage;
    private Function<DecryptedMqttMessage, Void> authCallback;

    private final String masterDid;

    public FidoAuth(FragmentActivity context, String homeServer, String masterDid) {
        appContext = context;
        api = new AuthApi(context, homeServer, masterDid);
        this.masterDid = masterDid;
        try {
            fidoClient = new Authenticator(context, true, false);
        } catch (VirgilException e) {
            Log.e(TAG, "Error initializing FIDO client authenticator.", e);
        }
    }

    public String getMasterDid() {
        return masterDid;
    }

    @Override
    public void onSuccess(AuthenticatorMakeCredentialOptions options, String challenge) {
        Log.d(TAG, "OnSuccess for register begin API callback.");
        lastKnownChallenge = challenge;
        registration.onBeginSuccess(options);
    }

    @Override
    public void onSuccess(AuthenticatorGetAssertionOptions options, String challenge) {
        Log.d(TAG, "OnSuccess for authenticate begin API callback.");
        lastKnownChallenge = challenge;
        authentication.onAuthBeginSuccess(options);
    }

    @Override
    public void onFailure(Call call, Exception e) {
        Log.e("FIDO-RA", "Failure during API call.", e);
        processing.postValue(false);
    }

    public void registerBegin(DidKeys signingKeys, RegistrationHandler registrationHandler) throws Exception {
        lastKnownSigningKeys = signingKeys;
        registration = registrationHandler;
        api.apiRegisterBegin(signingKeys, this);
    }

    public void authenticateBegin(DidKeys signingKeys, AuthenticationHandler authenticationHandler,
                                  DecryptedMqttMessage message, Function<DecryptedMqttMessage, Void> callback) throws Exception {
        lastKnownSigningKeys = signingKeys;
        authentication = authenticationHandler;
        latestMessage = message;
        authCallback = callback;
        api.apiAuthenticateBegin(signingKeys, message.toNotificationExtras(), this);
    }


    public Call vaultKeysBegin(DidKeys signingKeys, ResponseCallback responseCallback) throws Exception {
        lastKnownSigningKeys = signingKeys;
        return api.apiGetStoredPassword(signingKeys, responseCallback);

    }

    public Call passwordBegin(DidKeys signingKeys,String url, String key, ResponseCallback responseCallback) throws Exception {
        lastKnownSigningKeys = signingKeys;
        return api.apiGetPasswordDetails(signingKeys, url, key, responseCallback);

    }


    public Call mobileFormBegin(DidKeys signingKeys, JsonObject jsonObject, ResponseCallback responseCallback, int code) throws Exception {
        lastKnownSigningKeys = signingKeys;
        return api.apiGetDetails(signingKeys, jsonObject, responseCallback, code);

    }


    public Call mobileStoreBegin(DidKeys signingKeys, JsonObject jsonObject, ResponseCallback responseCallback, int code) throws Exception {
        lastKnownSigningKeys = signingKeys;
        return api.apiAddStore(signingKeys, jsonObject, responseCallback, code);

    }

    public Call mobileRemove(DidKeys signingKeys, String key, String category, String provider, ResponseCallback responseCallback, int code) throws Exception {
        lastKnownSigningKeys = signingKeys;
        return api.apiRemoveStore(signingKeys, key, category, provider, responseCallback, code);

    }

    public void registerComplete(AuthenticatorMakeCredentialOptions options) {
        processing.postValue(true);
        String challenge = lastKnownChallenge;
        CancellationSignal cancellationSignal = new CancellationSignal();
        appContext.runOnUiThread(() -> {
            try {
                fidoClient.makeCredential(options, appContext, response -> {
                    Log.d(TAG, "Attestation object from biometric callback is " + response);
                    try {
                        Call complete = api.apiRegisterComplete(lastKnownSigningKeys, challenge, response, registration);
                        registration.onCompleteStarted(complete);
                    } catch (Exception e) {
                        Log.e("FIDO-R", "Cannot call registerResponse", e);
                    }

                    return null;
                }, cancellationSignal);
            } catch (NoBiometricsError nbe) {
                registration.onNoBiometricsError(nbe);
            } catch (Exception e) {
                Log.e("FIDO-R", "Cannot call registerResponse", e);
            } finally {
                processing.postValue(false);
            }
        });
    }

    public void authenticateComplete(AuthenticatorGetAssertionOptions options) {
        processing.postValue(true);
        String challenge = lastKnownChallenge;
        try {
            fidoClient.getAssertion(options, latestMessage.toNotificationExtras(), credentialList -> credentialList.get(0), appContext, response -> {
                try {
                    Call complete = api.apiAuthenticateComplete(lastKnownSigningKeys, challenge, response, authentication);
                    Log.d("TAG", "authenticateComplete: "+complete);
                    authentication.onAuthCompleteStarted(complete);
                    if (authCallback != null) {
                        latestMessage.updateBody(response);
                        authCallback.apply(latestMessage);
                    }
                } catch (Exception e) {
                    Log.e("FIDO-A", "Cannot call authenticationResponse", e);
                } finally {
                    processing.postValue(false);
                }

                return null;
            }, null);
        } catch (WebAuthnException | VirgilException e) {
            Log.e(TAG, "Error while trying do biometric FIDO authentication", e);
        }
    }
}
