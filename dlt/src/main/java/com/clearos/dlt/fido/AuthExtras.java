package com.clearos.dlt.fido;

public class AuthExtras {
    public String method;
    public String messageId;
    public String reference;

    public AuthExtras(String _method, String _messageId, String _reference) {
        method = _method;
        messageId = _messageId;
        reference = _reference;
    }
}
