package com.clearos.dlt.fido;

public class AuthenticateOptions {
    public final String method;
    public final String messageId;
    public final String did;
    public final String reference;

    public AuthenticateOptions(String oDid, String oMethod, String oMessageId, String oReference) {
        method = oMethod;
        did = oDid;
        messageId = oMessageId;
        reference = oReference;
    }
}
