package com.clearos.dlt.fido;

import duo.labs.webauthn.models.AuthenticatorMakeCredentialOptions;
import okhttp3.Call;

public interface RegisterBeginCallback {
    void onSuccess(AuthenticatorMakeCredentialOptions options, String challenge);
    void onFailure(Call call, Exception e);
}
