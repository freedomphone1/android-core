package com.clearos.dlt.fido;

import androidx.annotation.NonNull;
import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Response;

public class AddHeaderInterceptor implements Interceptor {
    @Override @NonNull
    public Response intercept(Interceptor.Chain chain) throws IOException {
        return chain.proceed(
                chain.request().newBuilder()
                        .header("X-Requested-With", "XMLHttpRequest")
                        .build()
        );
    }
}
