package com.clearos.dlt.fido;

import android.content.Context;
import android.util.Log;

import androidx.fragment.app.FragmentActivity;

import com.clearos.dlt.DecryptedMqttMessage;
import com.clearos.dlt.DidKeys;
import com.clearos.dlt.R;
import com.clearos.dlt.SharedPrefs;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;

import duo.labs.webauthn.Authenticator;
import duo.labs.webauthn.exceptions.NoBiometricsError;
import duo.labs.webauthn.exceptions.VirgilException;
import duo.labs.webauthn.models.AuthenticatorGetAssertionOptions;
import duo.labs.webauthn.models.AuthenticatorMakeCredentialOptions;
import okhttp3.Call;

public class FidoAuthenticator extends Authenticator implements AuthenticationHandler, RegistrationHandler {
    private static final String TAG = "FidoAuthenticator";

    private static final String FIDO_CREDENTIALS_PREF_KEY = "fido%s/%s/%s";

    private final DidKeys signingKeys;
    private final String homeServer;
    private final FidoAuth fidoAuth;
    private final Context context;
    private final boolean isDemo;

    /**
     * Construct a WebAuthn authenticator backed by a credential safe and cryptography provider.
     *
     * @param signingKeys Keypair to use for authenticating FIDO requests.
     * @param authenticationRequired require user authentication via biometrics
     * @param strongboxRequired      require that keys are stored in HSM
     * @param isDemo When true, use the demo server instead of production.
     */
    public FidoAuthenticator(FragmentActivity context,
                             DidKeys signingKeys,
                             String homeServer,
                             boolean authenticationRequired,
                             boolean strongboxRequired,
                             boolean isDemo) throws VirgilException {
        super(context, authenticationRequired, strongboxRequired);
        this.context = context;
        this.signingKeys = signingKeys;
        this.homeServer = homeServer;
        this.isDemo = isDemo;

        if (isDemo)
            fidoAuth = new FidoAuth(context, context.getString(R.string.demoServer), signingKeys.getDid());
        else
            fidoAuth = new FidoAuth(context, homeServer, signingKeys.getDid());

        Set<String> credentials = getFidoCredentials(context.getApplicationContext(), signingKeys.getDid(), homeServer, isDemo);
        if (credentials == null || credentials.size() ==  0) {
            // We need to first register the phone as a FIDO authenticator.
            try {
                fidoAuth.registerBegin(signingKeys, this);
            } catch (Exception e) {
                Log.e(TAG, "Error initiating registration requst for FIDO.", e);
            }
        }
    }

    /**
     * Asks the user to approve the
     * @param message Push notification message to approve via FIDO.
     */
    public void authenticate(DecryptedMqttMessage message, Function<DecryptedMqttMessage, Void> callback) {
        try {
            fidoAuth.authenticateBegin(signingKeys, this, message, callback);
        } catch (Exception e) {
            Log.e(TAG, "Error while trying to begin authentication for FIDO message " + message);
        }
    }

    @Override
    public void onAuthBeginSuccess(AuthenticatorGetAssertionOptions options) {
        fidoAuth.authenticateComplete(options);
    }

    @Override
    public void onAuthCompleteStarted(Call apiCall) {
        Log.d(TAG, "API call for FIDO authentication has started.");
    }

    @Override
    public void onListCredentials(String source, List<Credential> credentials) {
        String toastText = "";
        switch (source) {
            case "getKeys":
                toastText = "Got " + credentials.size() + " credentials registered at home server.";
                break;
            case "registerComplete":
                saveFidoCredentials(context, fidoAuth.getMasterDid(), credentials, homeServer, isDemo);
                toastText = "FIDO credential registration complete.";
                break;
            case "authenticateComplete":
                toastText = "FIDO credential authentication complete.";
                break;
        }

        Log.i(TAG, toastText);
    }

    @Override
    public void onFailure(Call call, Exception e) {
        Log.e(TAG, "Failure while trying to FIDO authenticate.", e);
    }


    /**
     * Returns the prefs key to use for the serialized FIDO credentials. Includes the home server
     * host name so that if home server changes, the registration is triggered again.
     * @param homeServerHostName Host name of the home server to use.
     */
    private static String getFidoCredentialsPrefKey(String masterDid, String homeServerHostName, boolean isDemo) {
        String demoSuffix = isDemo ? "d" : "p";
        return String.format(FIDO_CREDENTIALS_PREF_KEY, demoSuffix, masterDid, homeServerHostName);
    }

    /**
     * Persists the list of Fido Credentials to application storage.
     */
    public static void saveFidoCredentials(Context appContext, String masterDid, List<Credential> credentials, String homeServerHostName, boolean isDemo) {
        SharedPrefs prefs = SharedPrefs.getInstance(appContext);
        prefs.saveSet(getFidoCredentialsPrefKey(masterDid, homeServerHostName, isDemo), toCredentialStringSet(credentials));
    }

    /**
     * Retrieves the list of saved FIDO credentials from app storage, if they exist.
     * @return Null if there aren't any saved credentials.
     */
    public static Set<String> getFidoCredentials(Context appContext, String masterDid, String homeServerHostName, boolean isDemo) {
        SharedPrefs prefs = SharedPrefs.getInstance(appContext);
        Set<String> credentials = prefs.getSet(getFidoCredentialsPrefKey(masterDid, homeServerHostName, isDemo));
        if (credentials != null) {
            Log.d(TAG, String.format("Found %d credentials: %s", credentials.size(), credentials));
        }
        return credentials;
    }

    private static Set<String> toCredentialStringSet(List<Credential> credentials) {
        Set<String> result = new HashSet<>();
        for (int i = 0; i < credentials.size(); i++) {
            result.add("$i;${credential.id};${credential.publicKey}");
        }
        return result;
    }

    @Override
    public void onBeginSuccess(AuthenticatorMakeCredentialOptions options) {
        Log.i(TAG, "onBeginSuccess for initial API server request.");
        fidoAuth.registerComplete(options);
    }

    @Override
    public void onCompleteStarted(Call apiCall) {
        Log.d(TAG, "Initial API request for FIDO registration has started.");
    }

    @Override
    public void onNoBiometricsError(NoBiometricsError error) {
        Log.e(TAG, "No biometrics available for FIDO.", error);
    }

    @Override
    public void onTimeoutError(TimeoutException e) {
        Log.e(TAG, "Request for FIDO registration timed out.", e);
    }
}
