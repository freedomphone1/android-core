package com.clearos.dlt;

public class PushNotificationResponse {
    private STATUS success;
    private NotificationMessageResponse[] successful;
    private NotificationMessageResponse[] failed;

    public STATUS getSuccess() {
        return success;
    }

    public NotificationMessageResponse[] getSuccessful() {
        return successful;
    }

    public NotificationMessageResponse[] getFailed() {
        return failed;
    }

    public enum STATUS {
        success,
        partial,
        failed
    }
}
