package com.clearos.dlt;

import android.content.Context;
import android.util.Log;

import com.google.gson.reflect.TypeToken;

import java.util.function.Function;

public class ApiCacheCombiner<T> extends DidAuthApiCallback<T> {
    private final ApiCache<T> apiCache;
    private final long duration;

    public ApiCacheCombiner(Context appContext, Function<T, Void> callback, String TAG, Class<T> typeParameterClass,
                            DidAuthApi client, String cacheFile, long duration, TypeToken<ApiCacheObject<T>> typeToken) {
        super(callback, TAG, typeParameterClass, client);
        apiCache = new ApiCache<>(appContext, cacheFile, typeToken);
        this.duration = duration;
    }

    @Override
    public void runCallback(T result, boolean fromCache) {
        if (result != null) {
            if (!fromCache)
                apiCache.cache(result, duration);
            else
                Log.d(getTAG(), "Not re-caching a result that already came from the cache!");
        } else
            Log.d(getTAG(), "Not caching result since it is null.");

        super.runCallback(result, false);
    }

    public boolean restore() {
        T result = apiCache.getObject();
        if (result != null) {
            Log.i(getTAG(), String.format("Obtained API call result for %s from file; returning %s", apiCache.getFileName(), result));
            runCallback(result, true);
            return true;
        } else {
            Log.d(getTAG(), "Performing regular PUT since the cache does not exist.");
            return false;
        }
    }
}
