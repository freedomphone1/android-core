package com.clearos.dlt;


import android.content.Intent;
import android.security.KeyChain;
import android.util.Log;
import android.util.Pair;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.SignatureException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Certificate {
    private static final String TAG = "Certificate";
    public static final String ENCRYPTION_CERT_ALIAS = "ClearLIFE Encryption Certificate";

    /**
     * Converts a PKCS12 keystore certificate to bytes.
     */
    private static byte[] pkcsToBytes(KeyStore keyStore, char[] password) {
        try (ByteArrayOutputStream b = new ByteArrayOutputStream()) {
            keyStore.store(b, password);
            return b.toByteArray();
        } catch (Exception e) {
            Log.e(TAG, "Error converting PKCS certificate store to byte stream.", e);
            return null;
        }
    }

    /**
     * Gets an intent to install a new, randomly generated certificate for the DLT encryption.
     * @return Null if there is an error generating one.
     */
    public static Intent getInstallIntent(String password) {
        Pair<KeyPair, KeyStore> cert = getSelfSignedCertificate(password);
        if (cert != null) {
            Intent result = KeyChain.createInstallIntent();
            result.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            result.putExtra(KeyChain.EXTRA_PKCS12, pkcsToBytes(cert.second, password.toCharArray()));
            result.putExtra(KeyChain.EXTRA_NAME, ENCRYPTION_CERT_ALIAS);
            return result;
        } else return null;
    }

    /**
     * Generates a self-signed certificate that can be imported into the system-wide keychain.
     * @return Generated, random key-pair and the corresponding self-certificate.
     */
    public static Pair<KeyPair, KeyStore> getSelfSignedCertificate(String password) {
        try {
            KeyPair keyPair = generateKeyPair();
            KeyStore certificate = generateCertificate(keyPair, password);
            return new Pair<>(keyPair, certificate);
        } catch (Exception e) {
            Log.e(TAG, "Error generating keypair or self-signed X509 certificate.");
            return null;
        }
    }

    private static KeyPair generateKeyPair() throws NoSuchAlgorithmException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance( "RSA" );
        SecureRandom random = SecureRandom.getInstance( "SHA1PRNG");
        keyGen.initialize( 2048, random );
        return keyGen.generateKeyPair();
    }

    private static KeyStore generateCertificate(KeyPair keyPair, String password)
            throws OperatorCreationException, KeyStoreException, InvalidKeyException, NoSuchAlgorithmException,
            NoSuchProviderException, SignatureException, java.security.cert.CertificateException, IOException {
        String issuerString = "C=US, O=org, OU=ClearLIFE Enryption Certificate";
        String subjectString = "C=US, O=org, OU=ClearLIFE Encryption Certificate";
        X500Name issuer = new X500Name( issuerString );
        BigInteger serial = BigInteger.ONE;
        Date notBefore = new Date();
        Date notAfter = new Date( System.currentTimeMillis() + TimeUnit.DAYS.toMillis(5000));
        X500Name subject = new X500Name( subjectString );
        PublicKey publicKey = keyPair.getPublic();
        JcaX509v3CertificateBuilder v3Bldr = new JcaX509v3CertificateBuilder( issuer,
                serial,
                notBefore,
                notAfter,
                subject,
                publicKey );
        X509CertificateHolder certHldr = v3Bldr
                .build( new JcaContentSignerBuilder( "SHA512WithRSA" ).setProvider( "AndroidOpenSSL" ).build( keyPair.getPrivate() ) );
        X509Certificate cert = new JcaX509CertificateConverter().setProvider( "AndroidOpenSSL" ).getCertificate( certHldr );

        cert.checkValidity( new Date() );
        cert.verify( keyPair.getPublic() );

        final KeyStore keyStore = KeyStore.getInstance("PKCS12");
        keyStore.load(null, password.toCharArray());
        keyStore.setKeyEntry(ENCRYPTION_CERT_ALIAS,
                keyPair.getPrivate(),
                password.toCharArray(),
                new X509Certificate[]{cert});
        return keyStore;
    }
}
