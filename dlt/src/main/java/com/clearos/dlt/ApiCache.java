package com.clearos.dlt;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

/**
 * Allows caching of an arbitrary API response to file for a specified cache period instead of
 * making the remote request.
 */
public class ApiCache<T> {
    private final String TAG;
    private static final String CACHE_DIR = "apiCache";
    private static final Gson gson = DidAuthApi.getGson();

    private final String fileName;
    private final Context appContext;
    private final Type collectionType;

    public ApiCache(Context appContext, String fileName, TypeToken<ApiCacheObject<T>> typeToken) {
        this.fileName = fileName;
        this.appContext = appContext;
        this.collectionType = typeToken.getType();

        this.TAG = String.format("ApiCache<%s>", collectionType);
    }

    public String getFileName() {
        return fileName;
    }

    public File getObjectFile() {
        File dir = new File(appContext.getFilesDir(), CACHE_DIR);
        if (!dir.exists()) {
            Log.d(TAG, "Auto-creating the cache directory at " + dir);
            if (!dir.mkdir()) {
                Log.w(TAG, "Unable to auto-create the cache directory.");
            }
        }

        return new File(dir, fileName);
    }

    /**
     * Caches the specified object to disk for the specified period.
     * @param object Object to cache to disk.
     * @param duration Number of millis for which this object is considered valid.
     */
    public void cache(T object, long duration) {
        Log.i(TAG, String.format("Caching %s to file for %d millis.", object, duration));
        long currentTime = System.currentTimeMillis();
        ApiCacheObject<T> o = new ApiCacheObject<>(currentTime, currentTime + duration, object);
        File target = getObjectFile();

        try {
            String json = gson.toJson(o);
            Files.write(target.toPath(), json.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            Log.e(TAG, "Error caching object to disk at " + target.getPath());
        }
    }

    /**
     * Attempts to restore the object from serialized JSON on disk. If not available, returns null.
     */
    public T getObject() {
        File target = getObjectFile();
        T result = null;
        String contents = null;

        if (target.exists()) {
            try {
                contents = new String(Files.readAllBytes(target.toPath()), StandardCharsets.UTF_8);
            } catch (IOException e) {
                Log.e(TAG, "Error while reading from existing cached file; clobbering cache at " + target.getPath(), e);
                if (!target.delete())
                    Log.w(TAG, "Unable to delete the bad cache file at " + target.getPath());
            }

            if (contents == null) {
                Log.d(TAG, "Cache file had no contents; returning before deserialization.");
                return null;
            }

            try {
                ApiCacheObject<T> cached = gson.fromJson(contents, collectionType);

                long now = System.currentTimeMillis();
                if (now > cached.getExpiresAt()) {
                    Log.i(TAG, String.format("Cached object at %s has expired; deleting now.", target.getPath()));
                    if (!target.delete()) {
                        Log.w(TAG, "Unable to delete expired object cache file at " + target.getPath());
                    }
                } else
                    result = cached.getObject();
            } catch (JsonSyntaxException f) {
                Log.e(TAG, String.format("Error parsing JSON %s from cached file at %s", contents, target.getPath()));
            }
        } else {
            Log.d(TAG, "No cached object to return from missing file at " + target.getPath());
        }

        return result;
    }
}
