package com.clearos.dlt;

public class ApiCacheObject<T> {
    private final long created;
    private final long expiresAt;
    private final T object;

    public ApiCacheObject(long created, long expiresAt, T object) {
        this.created = created;
        this.expiresAt = expiresAt;
        this.object = object;
    }

    public long getCreated() {
        return created;
    }

    public long getExpiresAt() {
        return expiresAt;
    }

    public T getObject() {
        return object;
    }
}
