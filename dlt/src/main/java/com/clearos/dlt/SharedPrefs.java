package com.clearos.dlt;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.Map;
import java.util.Set;

public class SharedPrefs {
    private static final String TAG = "SharedPrefs";
    private static SharedPrefs instance = new SharedPrefs();
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;

    private SharedPrefs() {} //prevent creating multiple instances by making the constructor private

    //The context passed into the getInstance should be application level context.
    @SuppressLint("CommitPrefEdits")
    public static SharedPrefs getInstance(Context context) {
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(context.getPackageName(), Activity.MODE_PRIVATE);
            editor = sharedPreferences.edit();
        }
        return instance;
    }

    /**
     * Clears all shared preferences that have keys matching the specified pattern.
     * @param pattern Part of the key that should be removed.
     */
    public void clearPattern(String pattern) {
        Map<String, ?> allEntries = sharedPreferences.getAll();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            if (entry.getKey().contains(pattern)) {
                Log.d(TAG, String.format("Removing shared preferences key %s.", entry.getKey()));
                editor.remove(entry.getKey());
            }
        }
        editor.apply();
    }

    public void saveBoolean(String key, boolean value) {
        editor.putBoolean(key, value);
        editor.apply();
    }

    /**
     * Saves a key-value pair to the app's private shared preferences.
     */
    public void saveValue(String key, String value) {
        editor.putString(key, value);
        editor.apply();
    }

    /**
     * Returns a value stored using `saveValue`.
     * @param key Key used to store the value.
     */
    public String getValue(String key) {
        return sharedPreferences.getString(key, null);
    }

    /**
     * Saves a key-value pair to the app's private shared preferences.
     */
    public void saveLong(String key, long value) {
        editor.putLong(key, value);
        editor.apply();
    }

    /**
     * Returns a value stored using `saveValue`.
     * @param key Key used to store the value.
     */
    public long getLong(String key) {
        return sharedPreferences.getLong(key, 0L);
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        return sharedPreferences.getBoolean(key, defaultValue);
    }


    /**
     * Saves a set of strings under a single key.
     */
    public void saveSet(String key, Set<String> value) {
        editor.putStringSet(key, value);
        editor.apply();
    }

    /**
     * Returns a set of strings stored using `saveSet`.
     */
    public Set<String> getSet(String key) {
        return sharedPreferences.getStringSet(key, null);
    }

    /**
     * Removes a value from shared preferences for a String or StringSet.
     */
    public void rmValue(String key) {
        editor.remove(key);
        editor.apply();
    }

    /**
     * Clears all shared preferences for the package.
     */
    public void clearAll() {
        editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
        Log.d("TAG", "data from local storage cleared: ");
    }

}
