package com.clearos.dlt;

public class PushNotification {
    private String title;
    private String body;
    private String message;
    private String did;
    private TOPICS[] topics;
    private String packageName;

    public PushNotification() {
        this.topics = new TOPICS[] {
            TOPICS.push
        };
    }

    public PushNotification setTitle(String title) {
        this.title = title;
        return this;
    }

    public PushNotification setBody(String body) {
        this.body = body;
        return this;
    }

    public PushNotification setMessage(String message) {
        this.message = message;
        return this;
    }

    /**
     * @param packageName Name of the package sending the message. Note that this is checked server
     *                    side; if the package name doesn't match the DID used for `didauth`, the
     *                    notification will not be sent.
     */
    public PushNotification setPackageName(String packageName) {
        this.packageName = packageName;
        return this;
    }

    public String getPackageName() {
        return packageName;
    }

    public PushNotification setDid(String did) {
        this.did = did;
        return this;
    }

    public PushNotification setTopics(TOPICS[] topics) {
        this.topics = topics;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public String getDid() {
        return did;
    }

    public String getMessage() {
        return message;
    }

    public enum TOPICS {
        push
    }
}
