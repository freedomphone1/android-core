package duo.labs.webauthn.util;

import android.os.CancellationSignal;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.biometric.BiometricPrompt;

import java.security.Signature;
import java.util.function.Function;

import duo.labs.webauthn.Authenticator;
import duo.labs.webauthn.exceptions.WebAuthnException;
import duo.labs.webauthn.models.AuthenticatorGetAssertionOptions;
import duo.labs.webauthn.models.AuthenticatorGetAssertionResult;
import duo.labs.webauthn.models.PublicKeyCredentialSource;

public class BiometricGetAssertionCallback extends BiometricPrompt.AuthenticationCallback {
    private static final String TAG = "BiometricGetAssertionCallback";

    private final Authenticator authenticator;
    private final AuthenticatorGetAssertionOptions options;
    private final PublicKeyCredentialSource selectedCredential;
    private final Function<AuthenticatorGetAssertionResult, Void> callback;
    private final CancellationSignal signal;

    public BiometricGetAssertionCallback(Authenticator authenticator,
                                         AuthenticatorGetAssertionOptions options,
                                         PublicKeyCredentialSource selectedCredential,
                                         Function<AuthenticatorGetAssertionResult, Void> callback,
                                         CancellationSignal signal) {
        super();
        this.authenticator = authenticator;
        this.options = options;
        this.selectedCredential = selectedCredential;
        this.callback = callback;
        this.signal = signal;
    }

    @Override
    public void onAuthenticationSucceeded(@NonNull BiometricPrompt.AuthenticationResult result) {
        super.onAuthenticationSucceeded(result);
        Log.d(TAG, "Authentication Succeeded");

        // retrieve biometric prompt-approved signature
        Signature signature = result.getCryptoObject().getSignature();

        AuthenticatorGetAssertionResult assertionResult;
        try {
            assertionResult = authenticator.getInternalAssertion(options, selectedCredential, signature);
        }
        catch (WebAuthnException exception) {
            Log.w(TAG, "Failed getInternalAssertion: " + exception.toString());
            onAuthenticationFailed();
            return;
        }
        callback.apply(assertionResult);
    }

    @Override
    public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
        super.onAuthenticationError(errorCode, errString);
        if (errorCode == BiometricPrompt.ERROR_NEGATIVE_BUTTON) {
            onAuthenticationCancelled();
        }
        Log.e(TAG, String.format("Authentication error; code = %d; msg = %s", errorCode, errString));
        callback.apply(null);
    }

    @Override
    public void onAuthenticationFailed() {
        // this happens on a bad fingerprint read -- don't cancel/error if this happens
        super.onAuthenticationFailed();
        Log.d(TAG, "authentication failed");
    }

    public void onAuthenticationCancelled() {
        Log.d(TAG, "authentication cancelled");
        if (signal != null)
            signal.cancel();

        callback.apply(null);
    }
}
