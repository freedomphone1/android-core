// IDecentralizedStore.aidl
package com.clearos.dstorage;

import com.clearos.dstorage.IRemoteServiceCallback;

interface IDecentralizedStore {
    /**
     * Requests that a file shared file FileProvider be stored in decentralized storage.
     * @param filename string with the `sharedDirectory/fileName`.
     * @param contentURI URI with which ClearCLOUD can grab the file to place in decentralized storage.
     * @param displayName name to associate with the file instead of using its CID.
     */
    oneway void store(String filename, String displayName, in Uri contentURI);

    /**
     * Requests a contentURI to retrieve a file from decentralized storage that was previously
     * stored.
     *  @param filename string with the `sharedDirectory/fileName` that the file was saved under
     *         using a call to `store` previously.
     */
    oneway void load(String filename);

    /**
     * Requests deletion of a file from decentralized storage.
     *  @param filename string with the `sharedDirectory/fileName` that the file was saved under
     *         using a call to `store` previously.
     */
    oneway void delete(String filename);

    /**
     * Registers the calling package with ClearCLOUD for decentralized storage service.
     */
    oneway void registerPackage(String displayName, String did);

    /**
     * Checks registration status of the calling package with ClearCLOUD.
     */
    oneway void getPackageRegistrationStatus();

    /**
     * Register a callback interface for when async storage operations complete.
     */
    oneway void registerCallback(IRemoteServiceCallback callback);
    /**
     * Unregister a callback interface for when async storage operations complete.
     */
    oneway void uregisterCallback(IRemoteServiceCallback callback);
}
