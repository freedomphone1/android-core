syntax = "proto3";
package storage;

option go_package = "github.com/storewise/clearcenter-sdk/server/pkg/storage";
option java_multiple_files = true;
option java_package = "tech.storewise.grpc_storage";
option java_outer_classname = "StorageProto";

import "google/protobuf/empty.proto";
import "google/protobuf/timestamp.proto";

message ObjectIdentifier {
    // bucket name of the object stored in.
    string bucket = 1;
    // storage class of the object.
    string storageClass = 2;
    // key of the object.
    string key = 3;
}

message Object {
    // body of the object.
    bytes body = 1;
}

message ObjectInfo {
    // key of the object.
    string key = 1;
    // file size of the object.
    int64 size = 2;
    // last modified time.
    google.protobuf.Timestamp lastModified = 3;
    // publicKeys of the hosts where the object is stored in.
    repeated string publicKeys = 4;
}

// Data needed to authenticate a request using cryptography and `didauth`.
message DidAuth {
    // URL of the server to check DID against for access. Will provide the specific REST API request
    // in a different document.
    string homeServer = 1;
    // Decentralized ID of the entity making the request. This is sent as part of the home server
    // request to validate approval *and* to check for public key.
    string did = 2;
    // Date at which the request was signed. This is used to verify recency of control over the
    // private key.
    google.protobuf.Timestamp date = 3;
    // The message whose signature appears in `signature`.
    string msg = 4;
    // Signature made with the private key corresponding to did. Verifies that this request
    // originated with the controller of DID.
    bytes signature = 5;
}

// GetAuditLog is the audit log format for Get operations.
message GetAuditLog {
    // id of the requested object.
    ObjectIdentifier id = 1;
    // size of the returned object.
    int64 size = 2;
}

// PutAuditLog is the audit log format for Put operations.
message PutAuditLog {
    // id of the uploaded object.
    ObjectIdentifier id = 1;
    // size of the uploaded object.
    int64 size = 2;
    // publicKeys of the hosts where the object is stored in.
    repeated string publicKeys = 3;
}

// ExistAuditLog is the audit log format for Exist operations.
message ExistAuditLog {
    // id of the request object identifier.
    ObjectIdentifier id = 1;
}

// ListAuditLog is the audit log format for List operations.
message ListAuditLog {
    // bucket name to list.
    string bucket = 1;
    // limits the response to keys that begin with the specified prefix.
    string prefix = 2;
    // specifies the key to start with when listing objects in a bucket.
    string marker = 3;
    // sets the maximum number of keys returned in the response.
    int64 maxSize = 4;
}

// DeleteAuditLog is the audit log format for Delete operations.
message DeleteAuditLog {
    // id of the deleted object identifier.
    ObjectIdentifier id = 1;
}

// AuditLog is the common audit log format.
message AuditLog {
    // timestamp is the date-time when the request received.
    google.protobuf.Timestamp timestamp = 1;
    // did is the DID of the requester.
    string did = 2;
    // error is the error message set if the request fails.
    string error = 3;

    // log is the audit log.
    oneof log {
        // get is the audit log set if the request is Get operation.
        GetAuditLog get = 4;
        // put is the audit log set if the request is Put operation.
        PutAuditLog put = 5;
        // exist is the audit log set if the request is Exist operation.
        ExistAuditLog exist = 6;
        // list is the audit log set if the request is List operation.
        ListAuditLog list = 7;
        // delete is the audit log set if the request is Delete operation.
        DeleteAuditLog delete = 8;
    }
}

message PutRequest {
    ObjectIdentifier id = 1;
    Object object = 2;
    DidAuth auth = 3;
}

message PutResponse {
    // size is the received byte size.
    int64 size = 1;
}

message GetRequest {
    ObjectIdentifier id = 1;
    DidAuth auth = 2;
}

message ExistRequest {
    ObjectIdentifier id = 1;
    DidAuth auth = 2;
}

message ListRequest {
    // bucket name to list.
    string bucket = 1;
    // storage class to list.
    string storageClass = 2;
    // limits the response to keys that begin with the specified prefix.
    string prefix = 3;
    // specifies the key to start with when listing objects in a bucket.
    string marker = 4;
    // sets the maximum number of keys returned in the response.
    int64 maxSize = 5;
    DidAuth auth = 6;
}

message ListResponse {
    // list of object information.
    repeated ObjectInfo objects = 1;
    // When response is truncated (the IsTruncated element value in the response is true),
    // you can use the key name in this field as marker in the subsequent request to get next set of objects.
    // Objects are listed in alphabetical order.
    string nextMarker = 2;
}

message DeleteRequest {
    ObjectIdentifier id = 1;
    DidAuth auth = 2;
}

message AuditLogsRequest{
    DidAuth auth = 1;
    // reserve specifies whether the gateway keeps log records.
    bool reserve = 2;
}

message AuditLogsResponse{
    repeated AuditLog logs = 1;
}

service Storage {
    // Get returns an object associated with a given object identifier.
    rpc Get (GetRequest) returns (Object) {
    }

    // Put stores a given object with a given identifier.
    rpc Put (PutRequest) returns (PutResponse) {
    }

    // Exist checks if an object associated with a given identifier exists and returns its object info.
    rpc Exist (ExistRequest) returns (ObjectInfo) {
    }

    // List retrieves object identifiers in a given bucket.
    rpc List (ListRequest) returns (ListResponse) {
    }

    // Delete deletes an object associated with a given object identifier.
    rpc Delete (DeleteRequest) returns (google.protobuf.Empty) {
    }

    // AuditLogs returns a list of audit logs recorded after the last request.
    rpc AuditLogs(AuditLogsRequest) returns (AuditLogsResponse){
    }
}