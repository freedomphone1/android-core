package com.clearos.updates;

import android.os.AsyncTask;
import android.os.CancellationSignal;
import android.util.Log;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.event.ProgressListener;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;

public class AwsPublicDownloader extends AsyncTask<Void,Float,Void> {
    private final String key;
    private final String bucket;
    private final AWSCredentialsProvider creds;
    private final Function<Float, Void> progressHandler;
    private final Function<File, Void> doneCallback;
    private final File outputTarget;
    private int httpRequestTimeout = 3000;
    private int maxRequestRetries = 2;
    private CancellationSignal signal;

    private static final String TAG = "AwsPublicDownloader";

    /**
     * Downloads a file from AWS S3.
     * @param bucket The bucket in S3 to download from.
     * @param key Key in that bucket to download from.
     * @param credentials AWS access credentials that has S3 write access.
     */
    public AwsPublicDownloader(String bucket, String key, AWSCredentialsProvider credentials,
                               Function<Float, Void> progressHandler, File outputTarget,
                               Function<File, Void> doneCallback, CancellationSignal signal) {
        this(bucket, key, credentials, progressHandler, outputTarget, doneCallback);
        this.signal = signal;
    }

    public void setHttpRequestTimeout(int httpRequestTimeout) {
        this.httpRequestTimeout = httpRequestTimeout;
    }

    public int getHttpRequestTimeout() {
        return httpRequestTimeout;
    }

    public void setMaxRequestRetries(int maxRequestRetries) {
        this.maxRequestRetries = maxRequestRetries;
    }

    public int getMaxRequestRetries() {
        return maxRequestRetries;
    }

    /**
     * Downloads a file from AWS S3.
     * @param bucket The bucket in S3 to download from.
     * @param key Key in that bucket to download from.
     * @param credentials AWS access credentials that has S3 write access.
     */
    public AwsPublicDownloader(String bucket, String key, AWSCredentialsProvider credentials,
                         Function<Float, Void> progressHandler, File outputTarget, Function<File, Void> doneCallback) {
        this.key = key;
        this.bucket = bucket;
        creds = credentials;
        this.progressHandler = progressHandler;
        this.doneCallback = doneCallback;
        this.outputTarget = outputTarget;

        // Make sure that the output target folders exist before we try and write to a file in a
        // non-existent folder.
        File folder = outputTarget.getParentFile();
        if (folder != null && !folder.exists() && folder.mkdirs()) {
            Log.i(TAG, "Made intermediate directories for " + outputTarget.getPath());
        }
    }

    private void setProgressListener(long totalBytes) {
        AtomicLong bytesProcessed = new AtomicLong();
        ProgressListener progressListener = progressEvent -> {
            bytesProcessed.addAndGet(progressEvent.getBytesTransferred());
            float percent = (float) (bytesProcessed.get() * 100.0 / totalBytes);
            Log.i(TAG, String.format("Completed: %.2f", percent));
            onProgressUpdate(percent);
        };
    }

    @Override
    protected void onProgressUpdate(Float... values) {
        super.onProgressUpdate(values);
        if (progressHandler != null)
            progressHandler.apply(values[0]);
    }

    private boolean cancellationOk() {
        if (signal == null) {
            return true;
        }

        return !signal.isCanceled();
    }

    /**
     * Saves the specified stream to the output target file using a 4K buffer.
     * @param input Object input stream from S3.
     */
    private void saveFile(InputStream input, long fileSize) {
        BufferedInputStream reader = new BufferedInputStream(input);
        try {
            FileOutputStream target = new FileOutputStream(outputTarget);
            int bufferSize = (int)Math.min(fileSize, 4096);
            byte[] buffer = new byte[bufferSize];
            int available = reader.available();
            if (available < bufferSize)
                Arrays.fill(buffer, (byte)0);
            long bytesDone = 0;
            int bytesRead = 0;

            while (available > 0 && (bytesRead = reader.read(buffer, 0, bufferSize)) > 0 && cancellationOk()) {
                if (bytesRead < bufferSize)
                    target.write(Arrays.copyOfRange(buffer, 0, bytesRead));
                else
                    target.write(buffer);
                available = reader.available();
                bytesDone += bytesRead;

                if (Math.floorMod(bytesDone, 1024*1024) == 0) {
                    onProgressUpdate(((float)bytesDone)/fileSize);
                }

                if (available < bufferSize) {
                    Arrays.fill(buffer, (byte) 0);
                }
            }
        } catch (IOException e) {
            Log.e(TAG, "Error reading from AWS object stream", e);
        }
    }

    @Override
    protected Void doInBackground(Void... params) {
        ClientConfiguration config = new ClientConfiguration();
        config.setConnectionTimeout(httpRequestTimeout);
        config.setMaxErrorRetry(maxRequestRetries);

        AmazonS3Client s3Client = new AmazonS3Client(creds, config);
        s3Client.setRegion(Region.getRegion(Regions.US_WEST_2));

        GetObjectRequest getRequest = new GetObjectRequest(bucket, key);

        S3Object getResponse = null;
        Log.i(TAG, String.format("Download %s from %s", key, bucket));
        try {
            // First get the total size we will be downloading and setup a progress listener.
            getResponse = s3Client.getObject(getRequest);
            ObjectMetadata metadata = getResponse.getObjectMetadata();
//            setProgressListener(metadata.getContentLength());
//            getRequest.setGeneralProgressListener(progressListener);

            // Then, actually download the object.
            saveFile(getResponse.getObjectContent(), metadata.getContentLength());
        } catch (Exception e) {
            Log.e(TAG, "Error trying to download file from S3.", e);
        } finally {
            // To ensure that the network connection doesn't remain open, close any open input streams.
            if (getResponse != null) {
                try {
                    getResponse.close();
                } catch (IOException e) {
                    Log.e(TAG, "Error closing the object stream from AWS.", e);
                }
            }
        }

        doneCallback.apply(outputTarget);
        return null;
    }
}
