package com.clearos.updates;

import java.util.regex.Pattern;

public class RepoXMLHandler {

    private static final Pattern OLD_FDROID_PERMISSION = Pattern.compile("[A-Z_]+");

    /**
     * It appears that the default Android permissions in android.Manifest.permissions
     * are prefixed with "android.permission." and then the constant name.
     * FDroid just includes the constant name in the apk list, so we prefix it
     * with "android.permission."
     *
     * @see <a href="https://gitlab.com/fdroid/fdroidserver/blob/1afa8cfc/update.py#L91">
     * More info into index - size, permissions, features, sdk version</a>
     */
    public static String fdroidToAndroidPermission(String permission) {
        if (OLD_FDROID_PERMISSION.matcher(permission).matches()) {
            return "android.permission." + permission;
        }

        return permission;
    }
}
