package com.clearos.updates;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.util.Log;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;

import java.io.File;
import java.util.function.Function;

public class AwsPublicUploader extends AsyncTask<Void,Float,Void> {
    private final String key;
    private final String bucket;
    private final AWSCredentialsProvider credentials;
    private final Function<Float, Void> progressHandler;
    private final Function<Boolean, Void> doneCallback;
    private final File outputTarget;

    private static final String TAG = "AwsPublicUploader";

    /**
     * Downloads a file from AWS S3.
     * @param bucket The bucket in S3 to download from.
     * @param key Key in that bucket to download from.
     * @param credentials AWS access credentials that has S3 write access.
     */
    public AwsPublicUploader(String bucket, String key, AWSCredentialsProvider credentials,
                             Function<Float, Void> progressHandler, File outputTarget, Function<Boolean, Void> doneCallback) {
        this.key = key;
        this.bucket = bucket;
        this.credentials = credentials;
        this.progressHandler = progressHandler;
        this.doneCallback = doneCallback;
        this.outputTarget = outputTarget;

        // Make sure that the output target folders exist before we try and write to a file in a
        // non-existent folder.
        File folder = outputTarget.getParentFile();
        if (folder != null && !folder.exists()) {
            Log.e(TAG, "File does not exist");
            doneCallback.apply(false);
        }
    }

    @Override
    protected void onProgressUpdate(Float... values) {
        super.onProgressUpdate(values);
        if (progressHandler != null)
            progressHandler.apply(values[0]);
    }

    @SuppressLint("LogNotSignal")
    @Override
    protected Void doInBackground(Void... params) {
        AmazonS3Client s3Client = new AmazonS3Client(credentials);
        s3Client.setRegion(Region.getRegion(Regions.US_WEST_2));
        PutObjectRequest putRequest = new PutObjectRequest(bucket, key, outputTarget);

        try {
            s3Client.putObject(putRequest);
        } catch (AmazonServiceException e) {
            Log.e(TAG, "The call was transmitted successfully, but Amazon S3 couldn't process it, so it returned an error response.", e);
            e.printStackTrace();
            doneCallback.apply(false);
            return null;
        } catch (Exception e) {
            Log.e(TAG, "Error trying to upload file to S3.", e);
            doneCallback.apply(false);
            return null;
        }

        doneCallback.apply(true);
        return null;
    }
}

