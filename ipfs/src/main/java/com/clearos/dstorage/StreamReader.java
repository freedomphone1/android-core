package com.clearos.dstorage;

import java.io.InputStream;

public interface StreamReader {
    BlockReader getParent();
    void onDownloaded(InputStream object, long contentLength);
    void onError(Throwable e);
    Boolean isCompleted();
    Throwable getError();
}
