package com.clearos.dstorage;

public interface StreamWriter {
    BlockWriter getParent();
    void onUploaded(long uploadLength);
    void onError(Throwable e);
}
