package com.clearos.dstorage;

import android.os.AsyncTask;
import android.util.Log;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

import java.io.InputStream;

public class AwsDownloader  extends AsyncTask<Void,Void,Void> implements Downloader {
    public String key;
    private String bucket;
    private AWSCredentialsProvider creds;
    private int blockId;
    private String cid;
    public StreamReader reader;

    /**
     * Uploads a block to AWS S3.
     * @param did DID of the user that will own the file.
     * @param cid CID (IPFS multihash) of the block.
     * @param _bucket An encryption context (related to the app for which data is being uploaded).
     * @param credentials AWS access credentials that has S3 write access.
     */
    public AwsDownloader(String did, String cid, String _bucket, AWSCredentialsProvider credentials,
                         StreamReader _reader, int blockId) {
        key = String.format("%s/%s", did, cid);
        this.cid = cid;
        bucket = _bucket;
        creds = credentials;
        reader = _reader;
        this.blockId = blockId;
    }

    @Override
    public String getCid() {
        return cid;
    }
    @Override
    public int getBlockId() {
        return blockId;
    }
    @Override
    public String getKey() {
        return key;
    }

    @Override
    public StreamReader getReader() {
        return reader;
    }

    @Override
    public void start() {
        this.execute();
    }

    @Override
    protected Void doInBackground(Void... params) {
        AmazonS3Client s3Client = new AmazonS3Client(creds);
        s3Client.setRegion(Region.getRegion(Regions.US_WEST_2));

        GetObjectRequest getRequest = new GetObjectRequest(bucket, key);
        Log.i("AWS-DOWN", String.format("Download %s from %s", key, bucket));
        long contentLength = 0;
        try {
            S3Object getResponse = s3Client.getObject(getRequest);
            InputStream myObjectBytes = getResponse.getObjectContent();
            contentLength = getResponse.getObjectMetadata().getContentLength();
            reader.onDownloaded(myObjectBytes, contentLength);
        } catch (Exception e) {
            reader.onError(e);
        }

        reader.getParent().checkDownload(this, contentLength);

        return null;
    }
}
