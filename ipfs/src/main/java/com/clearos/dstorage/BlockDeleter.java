package com.clearos.dstorage;

import android.os.CancellationSignal;

import java.util.function.Function;

public interface BlockDeleter {
    void onStart(BlockHashEncrypter decrypter,
                 Function<String, Void> doneCallback,
                 IProgressHandler progressHandler,
                 CancellationSignal signal);
    void onDeleted(String cid, int blockId);
    void onError(String cid, int blockIndex, Throwable t);
}
