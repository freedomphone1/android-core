package com.clearos.dstorage;

public interface IndexCreator {
    void start();
    String getKey();
    BlockWriter getWriter();
}
