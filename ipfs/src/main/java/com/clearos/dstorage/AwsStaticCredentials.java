package com.clearos.dstorage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;

/**
 * Represents a set of hard-coded, static AWS credentials from the library resource strings file.
 */
public class AwsStaticCredentials {
    private Context appContext;

    public AwsStaticCredentials(Context applicationContext) {
        appContext = applicationContext;
    }

    public AWSCredentialsProvider provider() {
        return new AWSCredentialsProvider() {
            @Override
            public AWSCredentials getCredentials() {
                return credentials();
            }

            @Override
            public void refresh() {

            }
        };
    }

    /**
     * Returns hard-coded credentials for S3 from the library resource strings.
     */
    @SuppressLint("NewApi")
    private AWSCredentials credentials() {
        final String accessKey = appContext.getResources().getString(R.string.AccessKeyId);
        final String secretKey = appContext.getResources().getString(R.string.SecretKey);

        return new AWSCredentials() {
            @Override
            public String getAWSAccessKeyId() {
                return accessKey;
            }

            @Override
            public String getAWSSecretKey() {
                return secretKey;
            }
        };
    }
}
