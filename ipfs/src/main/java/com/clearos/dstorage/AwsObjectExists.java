package com.clearos.dstorage;

import android.os.AsyncTask;
import android.util.Log;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;

public class AwsObjectExists extends AsyncTask<Void,Void,Void> implements ObjectExists {
    private AWSCredentialsProvider creds;
    private String key;
    private String cid;
    private String bucket;
    private ObjectExistsHandler query;
    private byte[] contents;
    private Boolean exists = null;
    private int blockId = -1;
    private Throwable error;

    private final static String TAG = "AwsObjectExists";

    public AwsObjectExists(String did, String _cid, String _bucket, AWSCredentialsProvider credentials,
                           ObjectExistsHandler queryObj, byte[] _contents, int blockId) {
        key = String.format("%s/%s", did, _cid);
        cid = _cid;
        bucket = _bucket;
        creds = credentials;
        query = queryObj;
        contents = _contents;
        this.blockId = blockId;
    }

    public void setBlockId(int blockId) {
        this.blockId = blockId;
    }
    @Override
    public int getBlockId() {
        return blockId;
    }

    @Override
    public byte[] getCipher() {
        return contents;
    }

    @Override
    public Boolean getExists() {
        return exists;
    }

    @Override
    public String getCid() {
        return cid;
    }

    @Override
    public Throwable getError() {
        return error;
    }

    @Override
    public void start() {
        if (query.checkExists()) {
            this.execute();
        } else {
            exists = null;
            query.onExistsCheck(this);
        }
    }

    @Override
    protected Void doInBackground(Void... params) {
        Log.d(TAG, String.format("Starting S3 exists check for %s and %s", bucket, key));
        try {
            AmazonS3Client s3Client = new AmazonS3Client(creds);
            s3Client.setRegion(Region.getRegion(Regions.US_WEST_2));
            exists = s3Client.doesObjectExist(bucket, key);
        } catch (Exception e) {
            error = e;
            Log.e(TAG, "Error querying AWS object existence.", e);
        }
        query.onExistsCheck(this);

        return null;
    }
}
