package com.clearos.dstorage;

public interface Deleter {
    String getKey();
    String getCid();
    int getBlockId();
    void start();
}
