package com.clearos.dstorage;

import java.util.Map;
import java.util.Set;

public interface StorageReporter {

    /**
     * Called once the initial file has been analyzed to determine block size.
     * @param blockCount Number of blocks that will be processed for the file.
     */
    void onAnalyzed(int blockCount);

    /**
     * Errors that occurred during upload/download of particular blocks.
     * @param errors Keys are `did/CID` hashes from IPFS.
     */
    void onLoadErrors(Map<String, Throwable> errors);

    /**
     * Content errors, where the upload/download was allegedly successful, but the number of bytes
     * read/written is different.
     * @param errors Keys are `did/CID` hashes from IPFS.
     */
    void onContentErrors(Map<String, Exception> errors);

    /**
     * Fires with a set of successful uploads/downloads.
     * @param file Block summary (DAG) and overall CID for the file.
     * @param completed Items are `did/CID` hashes from IPFS. Completed means that the upload/download
     *                  finished (in either success/faiure).
     */
    void onSuccess(BlockHashFile file, Set<String> completed);

    /**
     * Calls when there is an IO/encryption error during block processing so that the overall file
     * could not get processed.
     * @param completed Items are `did/CID` hashes from IPFS. Completed means that the upload/download
     *                  finished (in either success/faiure).
     */
    void onFailure(Throwable error, Set<String> completed);

    /**
     * Fires when a cancellation was requested.
     * @param completed Set of those blocks that completed upload/download before cancelling.
     */
    void onCancelled(Set<String> completed);
}
