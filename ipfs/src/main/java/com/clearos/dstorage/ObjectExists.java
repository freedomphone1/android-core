package com.clearos.dstorage;

public interface ObjectExists {
    void start();
    byte[] getCipher();
    String getCid();
    Boolean getExists();
    int getBlockId();
    Throwable getError();
}
