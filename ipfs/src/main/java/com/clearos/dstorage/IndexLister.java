package com.clearos.dstorage;

public interface IndexLister {
    void start();
    AwsLister getLister();
}
