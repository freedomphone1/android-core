package com.clearos.dstorage;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class ProgressResult implements Parcelable {
    // We purposely have the total blocks be float to calculate percents more easily.
    private float totalBlocks;
    private int blocksDone;
    private int blockIndex;
    private String cid;
    private long cipherLength = -1;
    private Throwable blockError;

    private String errorMessage;
    private String fileKey;
    private Long started;
    private Long runtime;

    private final static String TAG = "ProgressResult";

    public ProgressResult(String cid, int blockIndex, long cipherLength, int blocksDone, int totalBlocks) {
        this.cid = cid;
        this.blockIndex = blockIndex;
        this.cipherLength = cipherLength;
        this.blocksDone = blocksDone;
        this.totalBlocks = totalBlocks;
    }

    public ProgressResult(String cid, int blockIndex, int totalBlocks) {
        this(cid, blockIndex, -1, -1, totalBlocks);
    }

    public void setFileKey(String fileKey) {
        this.fileKey = fileKey;
    }
    public String getFileKey() {
        return fileKey;
    }

    public void setBlocksDone(int blocksDone) {
        Log.d(TAG, String.format("Setting blocks done to %d at index %d ", blocksDone, blockIndex));
        this.blocksDone = blocksDone;
    }

    public void setBlockError(Throwable blockError) {
        this.blockError = blockError;
        errorMessage = blockError.getMessage();
    }
    public Throwable getBlockError() {
        return blockError;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public Float getTotalPercentDone() {
        return blocksDone/totalBlocks;
    }

    public void setTotalBlocks(int totalBlocks) {
        this.totalBlocks = totalBlocks;
    }
    public int getTotalBlocks() {
        return (int)totalBlocks;
    }

    public long getCipherLength() {
        return cipherLength;
    }
    public int getBlocksDone() {
        return blocksDone;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }
    public String getCid() {
        return cid;
    }


    public void setStarted(Long started) {
        this.started = started;
    }
    public Long getStarted() {
        return started;
    }

    public void setRuntime(Long runtime) {
        this.runtime = runtime;
    }
    public Long getRuntime() {
        return runtime;
    }

    public void setBlockIndex(int blockIndex) {
        this.blockIndex = blockIndex;
    }
    public int getBlockIndex() {
        return blockIndex;
    }
    public ProgressResult(Parcel in) {
        totalBlocks = in.readInt();
        blocksDone = in.readInt();
        blockIndex = in.readInt();
        cid = in.readString();
        errorMessage = in.readString();
        fileKey = in.readString();
        cipherLength = in.readLong();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt((int)totalBlocks);
        dest.writeInt(blocksDone);
        dest.writeInt(blockIndex);
        dest.writeString(cid);
        dest.writeString(errorMessage);
        dest.writeString(fileKey);
        dest.writeLong(cipherLength);
    }

    public static final Parcelable.Creator<ProgressResult> CREATOR
            = new Parcelable.Creator<ProgressResult>() {
        public ProgressResult createFromParcel(Parcel in) {
            return new ProgressResult(in);
        }

        public ProgressResult[] newArray(int size) {
            return new ProgressResult[size];
        }
    };
}
