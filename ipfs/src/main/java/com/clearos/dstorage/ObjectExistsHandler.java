package com.clearos.dstorage;

public interface ObjectExistsHandler {
    /**
     * Fires after checking whether the specified object exists in the S3 bucket.
     */
    void onExistsCheck(ObjectExists checker);

    /**
     * Should the existence of an object be queried?
     * @return True if existence should be queried.
     */
    boolean checkExists();
    void onGenericError(Throwable error);
}
