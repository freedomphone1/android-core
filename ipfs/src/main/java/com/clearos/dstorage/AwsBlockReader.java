package com.clearos.dstorage;

import android.annotation.SuppressLint;
import android.os.CancellationSignal;
import android.util.Log;
import android.util.SparseArray;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.clearos.storage.SwtDownloader;
import com.clearos.storage.SwtGateway;
import com.clearos.storage.provider.DecentralizedStorage;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.cbor.databind.CBORMapper;
import com.goterl.lazysodium.exceptions.SodiumException;
import com.goterl.lazysodium.utils.KeyPair;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import io.ipfs.multihash.Multihash;

public class AwsBlockReader implements BlockReader  {

    private final ObjectMapper MAPPER =  CBORMapper.builder().build();
    private KeyPair baseKeys;
    private String fileCid;
    private String did;
    private AWSCredentialsProvider creds;
    private String bucket;
    private Map<String, Throwable> downloadErrors = new HashMap<>();
    private Map<String, Exception> contentErrors = new HashMap<>();
    private ConcurrentSkipListSet<String> completedDownloads = new ConcurrentSkipListSet<>();
    private StorageReporter reporter;
    private BlockHashEncrypter ipfsDecrypter;
    private BlockHashFile file = null;
    private OutputStream outFile;
    private int lastIndexWrite = -1;
    private SparseArray<byte[]> unwrittenBlocks = new SparseArray<>();
    private  boolean anyBlockFailed = false;
    private Function<BlockHashFile, Void> finishCallback;
    private IProgressHandler progressHandler;
    private CancellationSignal signal;
    private boolean isAWS = true;
    private SwtGateway gateway;

    private final Stack<Integer> concurrentBlocks = new Stack<>();
    private CountDownLatch blockCompletedLatch;

    private static final String TAG = "AwsBlockReader";

    public AwsBlockReader(KeyPair baseKeys, String _did, String cid, String _bucket, AWSCredentialsProvider credentials, StorageReporter _reporter) {
        fileCid = cid;
        this.baseKeys = baseKeys;
        did = _did;
        creds = credentials;
        reporter = _reporter;
        bucket = _bucket;
        isAWS = true;
    }

    public AwsBlockReader(KeyPair baseKeys, String _did, String cid, String _bucket, SwtGateway gateway, StorageReporter _reporter) {
        fileCid = cid;
        this.baseKeys = baseKeys;
        did = _did;
        reporter = _reporter;
        bucket = _bucket;
        isAWS = false;
        this.gateway = gateway;
    }

    @Override
    public KeyPair getBaseKeys() {
        return baseKeys;
    }

    public void onBegin(BlockHashEncrypter decrypter,
                        OutputStream writeTarget,
                        Function<BlockHashFile, Void> callback,
                        IProgressHandler progressHandler,
                        CancellationSignal signal) {
        // Download the index file from AWS and decrypt it.
        ipfsDecrypter = decrypter;
        outFile = writeTarget;
        finishCallback = callback;
        this.progressHandler = progressHandler;
        this.signal = signal;

        AwsReader reader = new AwsReader(fileCid, this);
        Downloader downloader = getDownloader(fileCid, reader, -1);
        downloader.start();
    }

    private Downloader getDownloader(String cid, StreamReader reader, int blockId) {
        if (isAWS) {
            return new AwsDownloader(did, cid, bucket, creds, reader, blockId);
        } else {
            return new SwtDownloader(gateway, did, cid, bucket, reader, blockId);
        }
    }

    /**
     * Returns a latch to synchronize on the number of blocks finished so far.
     */
    public CountDownLatch getBlockCompletedLatch() {
        return blockCompletedLatch;
    }

    public void onNext(String cid) {
        if (signal == null || !signal.isCanceled()) {
            int blockId = Arrays.asList(file.dag).indexOf(cid);
            AwsReader reader = new AwsReader(cid, this);
            Downloader downloader = getDownloader(cid, reader, blockId);
            downloader.start();
        }
    }
    public void onIOError(IOException error) {
        reporter.onFailure(error, completedDownloads);
    }
    public void onEncryptionError(SodiumException error) {
        reporter.onFailure(error, completedDownloads);
    }

    /**
     * Decrypts a downloaded stream from AWS. Note that since blocks are written to separated files,
     * we read them in one go instead of buffering.
     * @param cid CID of the block that is being decrypted.
     * @param download Download stream from S3.
     * @return Decrypted block contents.
     */
    private byte[] decryptStream(String cid, InputStream download, long length) {
        // If we have failed blocks, don't bother downloading any others.
        if (anyBlockFailed) {
            try {
                download.close();
            } catch (IOException e) {
                onIOError(e);
            }
            return null;
        }

        byte[] index = null;
        byte[] block = null;
        long bytesRead = 0;
        int offset = 0;
        int available = 0;
        try {
            index = new byte[(int)length];
            available = download.available();
            while (available > 0 && offset < length) {
                bytesRead = download.read(index, offset, available);
                if (bytesRead > -1) {
                    offset += bytesRead;
                    available = download.available();
                } else {
                    available = 0;
                }
            }

            if (offset != index.length) {
                contentErrors.put(fileCid, new Exception("Incorrect number of bytes read from download stream."));
                anyBlockFailed = true;
            }

            // If any of the previous blocks failed, we may as well close the stream and exit. No need
            // to waste CPU cycles decrypting when the file can't be reconstructed.
            download.close();
            if (!anyBlockFailed) {
                block = ipfsDecrypter.decryptBlock(baseKeys, index, cid);
            }
        } catch (IOException e) {
            anyBlockFailed = true;
            onIOError(e);
        } catch (SodiumException e) {
            anyBlockFailed = true;
            onEncryptionError(e);
        }

        if (anyBlockFailed) signal.cancel();

        return block;
    }

    private void startMeteredBlockDownload() {
        int blockSize = DecentralizedStorage.calculateBlockSize(file.size);
        int maxConcurrentBlocks = (int)ipfsDecrypter.concurrentBlockCount(blockSize);
        Log.d(TAG, String.format("Initializing stack for maximum of %d concurrent blocks.", maxConcurrentBlocks));

        blockCompletedLatch = new CountDownLatch(file.dag.length);
        int index = 0;
        try {
            while (index < file.dag.length && ipfsDecrypter.canProcessNewBlock(concurrentBlocks, maxConcurrentBlocks, 180, TimeUnit.SECONDS)) {
                String cid = file.dag[index];
                concurrentBlocks.push(index);
                onNext(cid);
                index++;
            }
        } catch (IOException e) {
            Log.e(TAG, "Error while waiting for space in download queue.", e);
        }
    }

    /**
     * Decrypts the block and places its contents in the appropriate place locally.
     * @param cid IPFS hash of the file block.
     * @param download stream contents of the block.
     */
    public void cacheBlock(String cid, InputStream download, long length) {
        // Whether or not we got the whole file, the download has terminated so record that.
        byte[] block = decryptStream(cid, download, length);

        // Since we were able to decrypt, we must have a good block of data; otherwise, don't try
        // to do the other stuff. Note that if a single block fails, we can't reconstruct the file
        // so we might as well quit.
        if (block == null) {
            finish();
            return;
        }

        if (fileCid.equals(cid)) {
            // This is the index file, deserialize the block into its DAG.
            try {
                Map<String,Object> responseMap = (Map<String,Object>) MAPPER.readValue(block, Map.class);
                Object _size = responseMap.getOrDefault("size", null);
                long size = -1;
                if (_size != null) size = (int)_size;

                List<String> _dag = (ArrayList<String>)responseMap.getOrDefault("dag", null);
                String[] dag = new String[_dag.size()];
                dag = _dag.toArray(dag);

                file = new BlockHashFile(Multihash.fromBase58(fileCid), dag, size, null);

                // Before we continue, let's verify that the CID matches the contents of this index
                responseMap.remove("headers");
                byte[] cborBytes = BlockHashEncrypter.toCbor(responseMap);
                Multihash indexCid = ipfsDecrypter.getCid(cborBytes);
                if (!indexCid.toBase58().equals(fileCid)) {
                    throw new IOException("Decrypted CID of index file doesn't match file CID.");
                }

                // Queue up the download of the other blocks.
                startMeteredBlockDownload();
            } catch (Exception e) {
                anyBlockFailed = true;
                reporter.onFailure(e, completedDownloads);
            }
        } else {
            // This is just a regular block that needs decrypted and assembled.
            int index = Arrays.asList(file.dag).indexOf(cid);
            if (index > -1) {
                // Check if this block is the next one in the order; if it is, write it out to the
                // the target file.
                if (index == lastIndexWrite + 1) {
                    try {
                        outFile.write(block);
                        outFile.flush();
                        lastIndexWrite += 1;
                    } catch (IOException e) {
                        onIOError(e);
                        anyBlockFailed = true;
                    }
                } else {
                    // We got the blocks out of order because it is all async. Cache this block to
                    // memory; then ook at blocks already in the cache and try to write them.
                    unwrittenBlocks.append(index, block);
                    byte[] current = unwrittenBlocks.get(lastIndexWrite + 1, null);
                    while (current != null && !anyBlockFailed) {
                        // We only enter this loop if we had the next block in the index.
                        unwrittenBlocks.remove(lastIndexWrite + 1);
                        try {
                            outFile.write(current);
                            outFile.flush();
                            lastIndexWrite += 1;
                            current = unwrittenBlocks.get(lastIndexWrite + 1, null);
                        } catch (IOException e) {
                            anyBlockFailed = true;
                            onIOError(e);
                        }
                    }
                }
            } else {
                contentErrors.put(cid, new Exception("Couldn't find CID in the index file."));
            }
        }

        finish();
    }


    @SuppressLint("DefaultLocale")
    private String getBlockUniqueKey(String key, int blockIndex) {
        return String.format("%s/%d", key, blockIndex);
    }

    /**
     * Checks the downloader for errors so that the calling application sstate can be synchronized.
     * @param downloader Download to check status on.
     */
    public void checkDownload(Downloader downloader, long contentLength) {
        StreamReader r = downloader.getReader();
        ProgressResult pr = new ProgressResult(
                downloader.getCid(),
                downloader.getBlockId(),
                contentLength, -1,
                file != null ? file.dag.length : -1);

        if (!r.isCompleted() && !(downloadErrors.containsKey(downloader.getKey()) || contentErrors.containsKey(downloader.getKey()))) {
            if (r.getError() != null) {
                downloadErrors.put(downloader.getKey(), r.getError());
                pr.setBlockError(r.getError());
            } else {
                Exception e = new Exception("Incorrect number of bytes downloaded.");
                contentErrors.put(downloader.getKey(), e);
                pr.setBlockError(e);
            }

            // If this is the index file, then we are stuck. The code will never proceed to run
            // the finishing logic that reports errors to the calling method.
            if (fileCid == null || downloader.getKey().contains(fileCid) || (signal != null && signal.isCanceled())) {
                reportAll();
            }
        }

        completedDownloads.add(downloader.getKey());
        pr.setBlocksDone(completedDownloads.size());
        if (progressHandler != null && file != null) {
            progressHandler.onBlockProgress(pr);
            progressHandler.onTotalProgress(pr.getTotalPercentDone());
        }

        Integer blockIndex;
        synchronized (concurrentBlocks) {
            if (concurrentBlocks.size() > 0)
                blockIndex = concurrentBlocks.pop();
            else
                blockIndex = -1;
        }
        if (blockIndex != downloader.getBlockId())
            Log.d(TAG, "Strange condition: block index from stack different than blockId from downloader.");
        if (blockCompletedLatch != null) // The very first "index" block doesn't work on the latch.
            blockCompletedLatch.countDown();
    }

    /**
     * Reports all errors and successes to the configured reporter. Closes the output file.
     */
    private void reportAll() {
        try {
            outFile.close();
            if (downloadErrors.size() > 0) reporter.onLoadErrors(downloadErrors);
            if (contentErrors.size() > 0) reporter.onContentErrors(contentErrors);
            if (signal != null && signal.isCanceled()) {
                reporter.onCancelled(completedDownloads);
            } else {
                reporter.onSuccess(file, completedDownloads);
            }
            if (anyBlockFailed)
                // The null callback result implies failure.
                finishCallback.apply(null);
            else
                finishCallback.apply(file);
        } catch (IOException e) {
            onIOError(e);
        }
    }

    /**
     * Runs finalizing logic on the file stream and reports errors and success.
     */
    private void finish() {
        // See if this was the last block that we needed to write to file.
        if (lastIndexWrite == file.dag.length-1 && unwrittenBlocks.size() == 0) {
            // We reconstructed the entire file. Close the stream and fire the callback.
            reportAll();
        } else if (signal != null && signal.isCanceled()) {
            reportAll();
        } else if (anyBlockFailed) reportAll();
    }
}
