package com.clearos.dstorage;

import android.os.CancellationSignal;
import com.goterl.lazysodium.exceptions.SodiumException;
import com.goterl.lazysodium.utils.KeyPair;

import java.io.IOException;
import java.io.OutputStream;
import java.util.function.Function;

public interface BlockReader {
    void onBegin(BlockHashEncrypter decrypter,
                 OutputStream writeTarget,
                 Function<BlockHashFile, Void> callback,
                 IProgressHandler progressCallback,
                 CancellationSignal signal);
    void onNext(String cid);
    void onIOError(IOException error);
    void onEncryptionError(SodiumException error);
    void checkDownload(Downloader download, long contentLength);
    KeyPair getBaseKeys();
}
