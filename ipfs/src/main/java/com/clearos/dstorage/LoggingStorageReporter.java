package com.clearos.dstorage;

import android.util.Log;

import java.util.Map;
import java.util.Set;

public class LoggingStorageReporter implements StorageReporter {
    @Override
    public void onContentErrors(Map<String, Exception> errors) {
        for (Map.Entry<String, Exception> e: errors.entrySet()) {
            Log.e(e.getKey(), e.getValue().getMessage(), e.getValue());
        }
    }

    @Override
    public void onAnalyzed(int blockCount) {
        Log.i("DSAN", String.format("Analysis of file completed, %d blocks.", blockCount));
    }

    @Override
    public void onFailure(Throwable error, Set<String> completed) {
        Log.e("DSFAIL", error.getMessage(), error);
    }

    @Override
    public void onLoadErrors(Map<String, Throwable> errors) {
        for (Map.Entry<String, Throwable> e: errors.entrySet()) {
            Log.e(e.getKey(), e.getValue().getMessage(), e.getValue());
        }
    }

    @Override
    public void onSuccess(BlockHashFile file, Set<String> completed) {
        if (file != null) {
            Log.i("DSOK", String.format("Successfully completed file download for %s", file.cid));
        } else {
            Log.e("DSERR", String.format("No BlockHashFile found on operation completion with %d blocks finished.", completed.size()));
        }
    }

    @Override
    public void onCancelled(Set<String> completed) {
        Log.i("DSCANCEL", "The decentralized storage function was cancelled.");
    }
}
