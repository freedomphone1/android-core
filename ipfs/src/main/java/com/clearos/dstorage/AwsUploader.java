package com.clearos.dstorage;


import android.os.AsyncTask;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;

import java.io.ByteArrayInputStream;

public class AwsUploader extends AsyncTask<Void,Void,Void> implements Uploader {

    private byte[] block;
    public String key;
    private String cid;
    private String bucket;
    private AWSCredentialsProvider creds;
    public StreamWriter writer;
    private int blockId;

    /**
     * Uploads a block to AWS S3.
     * @param _block Raw bytes to write to the AWS S3 bucket.
     * @param did DID of the user that will own the file.
     * @param cid CID (IPFS multihash) of the block.
     * @param _bucket An encryption context (related to the app for which data is being uploaded).
     * @param credentials AWS access credentials that has S3 write access.
     */
    public AwsUploader(byte[] _block, String did, String cid, String _bucket, AWSCredentialsProvider credentials,
                       StreamWriter _writer, int blockId) {
        block = _block;
        key = String.format("%s/%s", did, cid);
        bucket = _bucket;
        this.cid = cid;
        creds = credentials;
        writer = _writer;
        this.blockId = blockId;
    }

    @Override
    public String getCid() {
        return cid;
    }
    @Override
    public int getBlockId() {
        return blockId;
    }

    @Override
    public long getBlockLength() {
        return block.length;
    }

    @Override
    public StreamWriter getWriter() {
        return writer;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public void start() {
        this.execute();
    }

    @Override
    protected Void doInBackground(Void... params) {
        AmazonS3Client s3Client = new AmazonS3Client(creds);
        s3Client.setRegion(Region.getRegion(Regions.US_WEST_2));

        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(block.length);
        PutObjectRequest putRequest = new PutObjectRequest(bucket, key, new ByteArrayInputStream(block), metadata);
        PutObjectResult putResponse = null;
        try {
            putResponse = s3Client.putObject(putRequest);
            writer.onUploaded(block.length);
        } catch (Exception e) {
            writer.onError(e);
        }

        // Register completion with the block writer parent.
        writer.getParent().checkUpload(this);

        return null;
    }
}