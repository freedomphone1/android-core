package com.clearos.storage;

import android.util.Log;

import com.clearos.dstorage.BlockDeleter;
import com.clearos.dstorage.Deleter;
import com.google.protobuf.Empty;

import io.grpc.stub.StreamObserver;

public class SwtDeleter implements Deleter {
    public String key;
    private String bucket;
    private String cid;
    private SwtGateway gateway;
    private int blockId;
    private BlockDeleter deleter;

    private final static String TAG = "SwtDeleter";

    /**
     * Deletes a block from decentralized storage.
     * @param did DID of the user that will own the file.
     * @param cid CID (IPFS multihash) of the block.
     * @param _bucket An encryption context (related to the app for which data is being uploaded).
     */
    public SwtDeleter(SwtGateway gateway, String did, String cid, String _bucket,
                      BlockDeleter deleter, int blockId) {
        key = String.format("%s/%s", did, cid);
        this.cid = cid;
        bucket = _bucket;
        this.deleter = deleter;
        this.gateway = gateway;
        this.blockId = blockId;
    }

    @Override
    public String getCid() {
        return cid;
    }

    @Override
    public int getBlockId() {
        return blockId;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public void start() {
        gateway.getInstance().delete(bucket, key, new StreamObserver<Empty>() {
            @Override
            public void onNext(Empty value) {
                deleter.onDeleted(cid, blockId);
            }

            @Override
            public void onError(Throwable t) {
                deleter.onError(cid, blockId, t);
            }

            @Override
            public void onCompleted() {
                Log.d(TAG, "onCompleted fired for gRPC delete method.");
            }
        });
    }
}
