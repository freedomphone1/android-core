package com.clearos.storage;

import android.util.Log;

import com.clearos.dlt.DerivedKeyClient;

import java.io.File;
import java.io.IOException;

public class ReadOnlyPublicGateway {
    public static Gateway gateway = null;
    private static final File metadataDir = new File("/data/clearshare");
    private static final Object syncShutdown = new Object();
    private final static String TAG = "SwtGateway";

    /**
     * Creates a stand-alone gateway for read-only access to public buckets.
     * @throws Exception if starting the gateway fails.
     */
    public ReadOnlyPublicGateway(String contractSet) throws Exception {
        if (gateway == null) {
            gateway = new Gateway(contractSet);
            // This start method doesn't do anything if the gateway isn't already started. If it has
            // stopped, however, then it will be restarted.
            gateway.start(metadataDir.getAbsolutePath());
        }
    }

    /**
     * Creates a stand-alone gateway for read-only access to public buckets.
     * @throws Exception if starting the gateway fails.
     */
    public ReadOnlyPublicGateway() throws Exception {
       this(null);
    }

    public static Gateway getInstance() {
        return gateway;
    }

    /**
     * Shuts down the gateway client gracefully if it isn't already shutting down.
     */
    public void shutdown() {
        synchronized (syncShutdown) {
            if (gateway != null) {
                try {
                    stop();
                } catch (Exception e) {
                    Log.w(TAG, "Error shutting down decentralized storage gateway.");
                }
            }
        }
    }

    public void stop() throws Exception {
        gateway.stop();
        gateway = null;
    }
}
