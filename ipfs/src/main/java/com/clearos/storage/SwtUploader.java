package com.clearos.storage;

import com.clearos.dstorage.StreamWriter;
import com.clearos.dstorage.Uploader;

import io.grpc.stub.StreamObserver;
import tech.storewise.grpc_storage.PutResponse;

public class SwtUploader implements Uploader {

    private byte[] block;
    public String key;
    private String bucket;
    private StreamWriter writer;
    private int blockId;
    private String cid;
    private SwtGateway gateway;

    /**
     * Uploads a block to AWS S3.
     * @param _block Raw bytes to write to the AWS S3 bucket.
     * @param did DID of the user that will own the file.
     * @param cid CID (IPFS multihash) of the block.
     * @param _bucket An encryption context (related to the app for which data is being uploaded).
     */
    public SwtUploader(byte[] _block, String did, String cid, String _bucket, SwtGateway gateway,
                       StreamWriter _writer, int blockId) {
        block = _block;
        key = String.format("%s/%s", did, cid);
        this.cid = cid;
        bucket = _bucket;
        writer = _writer;
        this.blockId = blockId;
        this.gateway = gateway;
    }

    @Override
    public String getCid() {
        return cid;
    }

    @Override
    public int getBlockId() {
        return blockId;
    }

    @Override
    public StreamWriter getWriter() {
        return writer;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public long getBlockLength() {
        return block.length;
    }

    /**
     * Starts the block upload.
     */
    public void start() {
        gateway.getInstance().upload(bucket, key, block, new StreamObserver<PutResponse>() {
            @Override
            public void onNext(PutResponse value) {
                writer.onUploaded(value.getSize());
            }

            @Override
            public void onError(Throwable t) {
                writer.onError(t);
            }

            @Override
            public void onCompleted() {
                writer.getParent().checkUpload(SwtUploader.this);
            }
        });
    }
}
