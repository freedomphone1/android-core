package com.clearos.storage;

import com.clearos.dstorage.Downloader;
import com.clearos.dstorage.StreamReader;
import com.google.protobuf.ByteString;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import io.grpc.stub.StreamObserver;
import tech.storewise.grpc_storage.Object;

public class SwtDownloader implements Downloader {
    public String key;
    private String bucket;
    private String cid;
    private StreamReader reader;
    private SwtGateway gateway;
    private int blockId;

    /**
     * Uploads a block to AWS S3.
     * @param did DID of the user that will own the file.
     * @param cid CID (IPFS multihash) of the block.
     * @param _bucket An encryption context (related to the app for which data is being uploaded).
     */
    public SwtDownloader(SwtGateway gateway, String did, String cid, String _bucket,
                         StreamReader _reader, int blockId) {
        key = String.format("%s/%s", did, cid);
        this.cid = cid;
        bucket = _bucket;
        reader = _reader;
        this.gateway = gateway;
        this.blockId = blockId;
    }

    @Override
    public String getCid() {
        return cid;
    }

    @Override
    public int getBlockId() {
        return blockId;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public StreamReader getReader() {
        return reader;
    }

    @Override
    public void start() {
        gateway.getInstance().download(bucket, key, new StreamObserver<Object>() {
            @Override
            public void onNext(Object value) {
                ByteString body = value.getBody();
                InputStream result = new ByteArrayInputStream(body.toByteArray());
                reader.onDownloaded(result, body.size());
            }

            @Override
            public void onError(Throwable t) {
                reader.onError(t);
                reader.getParent().checkDownload(SwtDownloader.this, 0);
            }

            @Override
            public void onCompleted() {
                reader.getParent().checkDownload(SwtDownloader.this, 0);
            }
        });
    }
}
