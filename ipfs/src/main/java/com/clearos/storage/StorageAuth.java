package com.clearos.storage;

import com.google.gson.Gson;

public class StorageAuth {
    private String limitations;
    private String did;
    private String verkey;
    private String rawResponse;

    public void setRawResponse(String rawResponse) {
        this.rawResponse = rawResponse;
    }

    public String getRawResponse() {
        return rawResponse;
    }

    public String getDid() {
        return did;
    }

    public String getVerkey() {
        return verkey;
    }

    public StorageConfig getLimitations() {
        Gson gson = new Gson();
        return gson.fromJson(limitations, StorageConfig.class);
    }
}
