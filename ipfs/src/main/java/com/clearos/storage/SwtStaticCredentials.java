package com.clearos.storage;

import android.content.Context;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.clearos.dstorage.R;

public class SwtStaticCredentials {
    private Context appContext;

    public SwtStaticCredentials(Context applicationContext) {
        appContext = applicationContext;
    }

    public AWSCredentialsProvider provider() {
        return new AWSCredentialsProvider() {
            @Override
            public AWSCredentials getCredentials() {
                return credentials();
            }

            @Override
            public void refresh() {

            }
        };
    }

    /**
     * Returns hard-coded credentials for S3 from the library resource strings.
     */
    private AWSCredentials credentials() {
        final String accessKey = appContext.getResources().getString(R.string.SwtUsername);
        final String secretKey = appContext.getResources().getString(R.string.SwtPassword);
        return new AWSCredentials() {
            @Override
            public String getAWSAccessKeyId() {
                return accessKey;
            }

            @Override
            public String getAWSSecretKey() {
                return secretKey;
            }
        };
    }
}
