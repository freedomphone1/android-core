package com.clearos.storage.model;

import androidx.lifecycle.ViewModel;

import com.clearos.storage.db.DsFileDeleteLogs;
import com.clearos.storage.db.DsFileExistLogs;
import com.clearos.storage.db.DsFileGetLogs;
import com.clearos.storage.db.DsFileListLogs;
import com.clearos.storage.db.DsFilePutLogs;
import com.clearos.storage.db.DsLogDataSource;
import com.clearos.storage.db.ExistAuditLog;
import com.clearos.storage.db.GetAuditLog;
import com.clearos.storage.db.ListAuditLog;
import com.clearos.storage.db.DeleteAuditLog;
import com.clearos.storage.db.PutAuditLog;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;

public class DsLogViewModel extends ViewModel {
    private final DsLogDataSource dataSource;

    public DsLogViewModel(DsLogDataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Add one or more logs resulting from GET operations to the decentralized storage gateway.
     */
    Completable addGetLogs(GetAuditLog... getLogs) {
        return dataSource.addGetLogs(getLogs);
    }

    /**
     * Add one or more logs resulting from PUT operations to the decentralized storage gateway.
     */
    Completable addPutLogs(PutAuditLog... putLogs) {
        return dataSource.addPutLogs(putLogs);
    }

    /**
     * Add one or more logs resulting from EXIST operations to the decentralized storage gateway.
     */
    Completable addExistLogs(ExistAuditLog... existLogs) {
        return dataSource.addExistLogs(existLogs);
    }

    /**
     * Add one or more logs resulting from LIST operations to the decentralized storage gateway.
     */
    Completable addListLogs(ListAuditLog... listLogs) {
        return dataSource.addListLogs(listLogs);
    }

    /**
     * Add one or more logs resulting from DELETE operations to the decentralized storage gateway.
     */
    Completable addDeleteLogs(DeleteAuditLog... deleteLogs) {
        return dataSource.addDeleteLogs(deleteLogs);
    }

    /**
     * Returns a list of the most recent GET audit logs against the decentralized server. These GET
     * operations may originate on this device or another.
     * @param cid CID of the file to get logs for.
     * @param limit Maximum number of log entries to return.
     */
    Flowable<List<DsFileGetLogs>> listGetAuditLogs(String cid, int limit) {
        return dataSource.listGetAuditLogs(cid, limit);
    }

    /**
     * Returns a list of the most recent PUT audit logs against the decentralized server. These PUT
     * operations may originate on this device or another.
     * @param cid CID of the file to get logs for.
     * @param limit Maximum number of log entries to return.
     */
    Flowable<List<DsFilePutLogs>> listPutAuditLogs(String cid, int limit) {
        return dataSource.listPutAuditLogs(cid, limit);
    }

    /**
     * Returns a list of the most recent EXIST audit logs against the decentralized server. These EXIST
     * operations may originate on this device or another.
     * @param cid CID of the file to get logs for.
     * @param limit Maximum number of log entries to return.
     */
    Flowable<List<DsFileExistLogs>> listExistAuditLogs(String cid, int limit) {
        return dataSource.listExistAuditLogs(cid, limit);
    }

    /**
     * Returns a list of the most recent LIST audit logs against the decentralized server. These LIST
     * operations may originate on this device or another.
     * @param cid CID of the file to get logs for.
     * @param limit Maximum number of log entries to return.
     */
    Flowable<List<DsFileListLogs>> listListAuditLogs(String cid, int limit) {
        return dataSource.listListAuditLogs(cid, limit);
    }

    /**
     * Returns a list of the most recent DELETE audit logs against the decentralized server. These DELETE
     * operations may originate on this device or another.
     * @param cid CID of the file to get logs for.
     * @param limit Maximum number of log entries to return.
     */
    Flowable<List<DsFileDeleteLogs>> listDeleteAuditLogs(String cid, int limit) {
        return dataSource.listDeleteAuditLogs(cid, limit);
    }
}