package com.clearos.storage.model;

import androidx.lifecycle.ViewModel;
import androidx.room.Query;

import com.clearos.storage.db.DsFile;
import com.clearos.storage.db.DsFileDataSource;
import com.clearos.storage.db.DsFileName;
import com.clearos.storage.db.DsFileWithNames;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

public class DsFileViewModel extends ViewModel {
    private final DsFileDataSource dataSource;

    public DsFileViewModel(DsFileDataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Add one or more files to the local decentralized storage meta database.
     */
    Completable addFiles(DsFile... files) {
        return dataSource.addFiles(files);
    }

    /**
     * Add one or more aliases or additional names to files in the decentralized storage meta database.
     */
    Completable addFileNames(DsFileName... filenames) {
        return dataSource.addFileNames(filenames);
    }

    /**
     * Returns the filenames for a given cid
     *
     * @param cid the cid of the file
     * @return list of rowid's for given filenames
     */
    Flowable<List<DsFileName>> listFileNamesForCid(String cid) {
        return dataSource.listFileNamesForCid(cid);
    }

    /**
     * Lists the most recent files added to the decentralized storage.
     * @param limit Maximum number of files to return.
     */
    Flowable<List<DsFileName>> listRecentFiles(String packageName, int limit) {
        return dataSource.listRecentFiles(packageName, limit);
    }
}
