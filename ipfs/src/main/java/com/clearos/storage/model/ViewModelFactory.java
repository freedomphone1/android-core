package com.clearos.storage.model;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.clearos.storage.db.DsAppDataSource;
import com.clearos.storage.db.DbQueueDataSource;
import com.clearos.storage.db.DsFileDataSource;
import com.clearos.storage.db.DsLogDataSource;

/**
 * Factory for ViewModels
 */
public class ViewModelFactory implements ViewModelProvider.Factory {
    private DsAppDataSource appDataSource;
    private DbQueueDataSource queueDataSource;
    private DsFileDataSource fileDataSource;
    private DsLogDataSource logDataSource;

    public ViewModelFactory(DsAppDataSource dataSource) {
        appDataSource = dataSource;
    }
    public ViewModelFactory(DbQueueDataSource dataSource) {
        queueDataSource = dataSource;
    }
    public ViewModelFactory(DsFileDataSource dataSource) {
        fileDataSource = dataSource;
    }
    public ViewModelFactory(DsLogDataSource dataSource) {
        logDataSource = dataSource;
    }


    @Override
    @NonNull
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(DsFileViewModel.class)) {
            return (T) new DsFileViewModel(fileDataSource);
        } else if (modelClass.isAssignableFrom(DsAppViewModel.class)) {
            return (T) new DsAppViewModel(appDataSource);
        } else if (modelClass.isAssignableFrom(DsLogViewModel.class)) {
            return (T) new DsLogViewModel(logDataSource);
        } else if (modelClass.isAssignableFrom(DbQueueViewModel.class)) {
            return (T) new DbQueueViewModel(queueDataSource);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
