package com.clearos.storage.db;

import com.clearos.storage.provider.DecentralizedStoreOperation;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;

public interface DbQueueDataSource {

    /**
     * Add one or more file add operations to the local database queue.
     */
    Completable addQueueFiles(DbQueue... queueFiles);

    /**
     * Add one or more file update operations to the local database queue.
     */
    Completable updateQueueFiles(DbQueue... queueFiles);

    /**
     * Gets a single queue file from the database using its primary key fields.
     */
    Flowable<List<DbQueue>> getQueueFiles(DecentralizedStoreOperation op, String... filepaths);

    /**
     * Lists pending file operations that are queued in the database, but haven't been executed or
     * completed by the decentralized storage provider.
     * @param op Operation that was queued; one of `put`, `get`, `del`.
     * @param limit Maximum number of pending file operations to return.
     */
    Flowable<List<DbQueue>> pendingFiles(DecentralizedStoreOperation op, int limit);

    /**
     * Lists failed file operations from the database; these were started but failed or were interrupted.
     * @param op Operation that was queued; one of `put`, `get`, `del`.
     * @param limit Maximum number of failed file operations to return.
     */
    Flowable<List<DbQueue>> failedFiles(DecentralizedStoreOperation op, int limit, int maxRetries);

    /**
     * Lists completed file operations that were queued in the database.
     * @param op Operation that was queued; one of `put`, `get`, `del`.
     * @param limit Maximum number of completed file operations to return.
     */
    Flowable<List<DbQueue>> finishedFiles(DecentralizedStoreOperation op, int limit);

    /**
     * Lists running file operations that were queued in the database.
     * @param op Operation that was queued; one of `put`, `get`, `del`.
     * @param limit Maximum number of running file operations to return.
     */
    Flowable<List<DbQueue>> runningFiles(DecentralizedStoreOperation op, int limit);

}
