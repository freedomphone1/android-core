package com.clearos.storage.db;

import androidx.room.Embedded;
import androidx.room.Junction;
import androidx.room.Relation;

import java.util.Date;
import java.util.List;

public class DsFileWithNames {
    @Embedded
    public DsFile file;
    @Relation(
            parentColumn = "cid",
            entityColumn = "rowid",
            associateBy = @Junction(DsFileCrossRefDsFileName.class)
    )
    public List<DsFileName> fileNames;

    private int getLatestId() {
        long latest = 0;
        int i = 0, latestIndex = 0;
        for (DsFileName f : fileNames) {
            if (f.touched.getTime() > latest) {
                latest = f.touched.getTime();
                latestIndex = i;
            }
            i++;
        }

        return latestIndex;
    }

    /**
     * Gets the latest modified/created time stamp for the CID.
     *
     * @return
     */
    public Date getLatestTouched() {
        return fileNames.get(getLatestId()).touched;
    }

    /**
     * Gets the latest file name used for the CID.
     */
    public String getLatestName() {
        return fileNames.get(getLatestId()).filename;
    }
}
