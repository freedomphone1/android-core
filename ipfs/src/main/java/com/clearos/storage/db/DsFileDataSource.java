package com.clearos.storage.db;

import androidx.room.Query;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

public interface DsFileDataSource {

    /**
     * Add one or more files to the local decentralized storage meta database.
     */
    Completable addFiles(DsFile... files);

    /**
     * Add one or more aliases or additional names to files in the decentralized storage meta database.
     */
    Completable addFileNames(DsFileName... filenames);

    /**
     * Update one or more existing filenames
     */
    Completable updateFileNames(DsFileName... fileNames);

    /**
     * Add one or more Connections for files to filenames to decentralized storage meta database.
     */
    Completable addFileCrossRefFileNames(DsFileCrossRefDsFileName... dsFileCrossRefDsFileNames);


    /**
     * Returns a single filename if exists in database
     * @param fileName the name of file you are searching for
     * @param packageName the name of the calling application
     * @return DsFileName if exists otherwise null
     */
    Single<List<DsFileName>> getSingleFileName(String packageName, String fileName);

    Single<List<DsFileCrossRefDsFileName>> getCrossRefs();

    /**
     * Returns a list of cid's from cross reference table
     *
     * @param fileNameId the id for the filename
     * @return list of Strings of matching cid's
     */
    Single<List<String>> getCids(int fileNameId);

    /**
     * Returns a single file if exists in database
     * @param cid the content crypto string for specific file
     * @return DsFile if exists otherwise null
     */
    Single<List<DsFile>> getSingleFile(String cid);

    /**
     * Returns a list of Files for a given set of cid's
     *
     * @param cids list of cids to search for
     * @return a list of files
     */
    Single<List<DsFile>> getFiles(List<String> cids);

    /**
     * Searches the decentralized storage *metadata* by filename for files of a related CID.
     * This should be up-to-date if a sync has happened recently.
     * @param fileName file name of the file to search for.
     */
    Flowable<List<DsFileNamesWithFiles>> searchFilesbyName(String packageName, String fileName, int limit);

    /**
     * Returns the filenames for a given cid
     *
     * @param cid the cid of the file
     * @return list of rowid's for given filenames
     */
    Flowable<List<DsFileName>> listFileNamesForCid(String cid);

    /**
     * Lists the most recent files added to the decentralized storage.
     * @param limit Maximum number of files to return.
     */
    Flowable<List<DsFileName>> listRecentFiles(String packageName, int limit);
}
