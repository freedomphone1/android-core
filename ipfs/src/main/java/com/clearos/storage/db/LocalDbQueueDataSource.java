package com.clearos.storage.db;

import com.clearos.storage.provider.DecentralizedStoreOperation;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;

public class LocalDbQueueDataSource implements DbQueueDataSource {
    private DsDao dao;

    public LocalDbQueueDataSource(DsDao dao) { this.dao = dao; }

    /**
     * Add one or more file add operations to the local database queue.
     */
    @Override
    public Completable addQueueFiles(DbQueue... queueFiles) {
        return dao.addQueueFiles(queueFiles);
    }

    /**
     * Add one or more file update operations to the local database queue.
     */
    @Override
    public Completable updateQueueFiles(DbQueue... queueFiles) {
        return dao.updateQueueFiles(queueFiles);
    }

    /**
     * Gets a single queue file from the database using its primary key fields.
     */
    @Override
    public Flowable<List<DbQueue>> getQueueFiles(DecentralizedStoreOperation op, String... filepaths) {
        return dao.getQueueFiles(op, filepaths);
    }

    /**
     * Lists pending file operations that are queued in the database, but haven't been executed or
     * completed by the decentralized storage provider.
     *
     * @param op    Operation that was queued; one of `put`, `get`, `del`.
     * @param limit Maximum number of pending file operations to return.
     */
    @Override
    public Flowable<List<DbQueue>> pendingFiles(DecentralizedStoreOperation op, int limit) {
        return dao.pendingFiles(op, limit);
    }

    /**
     * Lists failed file operations from the database; these were started but failed or were interrupted.
     *
     * @param op         Operation that was queued; one of `put`, `get`, `del`.
     * @param limit      Maximum number of failed file operations to return.
     */
    @Override
    public Flowable<List<DbQueue>> failedFiles(DecentralizedStoreOperation op, int limit, int maxRetries) {
        return dao.failedFiles(op, limit, maxRetries);
    }

    /**
     * Lists completed file operations that were queued in the database.
     *
     * @param op    Operation that was queued; one of `put`, `get`, `del`.
     * @param limit Maximum number of completed file operations to return.
     */
    @Override
    public Flowable<List<DbQueue>> finishedFiles(DecentralizedStoreOperation op, int limit) {
        return dao.finishedFiles(op, limit);
    }

    /**
     * Lists running file operations that were queued in the database.
     *
     * @param op    Operation that was queued; one of `put`, `get`, `del`.
     * @param limit Maximum number of running file operations to return.
     */
    @Override
    public Flowable<List<DbQueue>> runningFiles(DecentralizedStoreOperation op, int limit) {
        return dao.runningFiles(op, limit);
    }
}
