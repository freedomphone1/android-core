package com.clearos.storage.db;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

import com.clearos.storage.provider.DecentralizedStorage;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.Date;

@Entity(tableName = "files")
public class DsFile {
    @PrimaryKey
    @NonNull
    public String cid;
    public Date created;
    public long size;
    public String mimeType;
    public boolean isDirectory;
    public String children;
    public String prefix;

    public DsFile(@NotNull String cid) {
        this.cid = cid;
    }

    public File getCidFile(DecentralizedStorage storage) {
        return new File(storage.getApplicationContext().getCacheDir(), cid);
    }

    /**
     * Returns the CID for all children in this DsFile if it is a directory.
     */
    public String[] getChildren() {
        if (isDirectory) {
            return children.split(",");
        } else {
            return null;
        }
    }
}
