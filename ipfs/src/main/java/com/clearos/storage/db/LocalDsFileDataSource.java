package com.clearos.storage.db;

import android.util.Log;

import androidx.room.Query;

import java.util.Arrays;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

public class LocalDsFileDataSource implements DsFileDataSource {
    private final DsDao dao;

    public LocalDsFileDataSource(DsDao dao) {
        this.dao = dao;
    }


    /**
     * Add one or more files to the local decentralized storage meta database.
     */
    @Override
    public Completable addFiles(DsFile... files) {

        Log.d("Dao", "Adding DsFile of: " +
                Arrays.stream(files).iterator().next().toString());
        return dao.addFiles(files);
    }

    /**
     * Add one or more aliases or additional names to files in the decentralized storage meta database.
     */
    @Override
    public Completable addFileNames(DsFileName... filenames) {
        Log.d("Dao", "Adding DsFileName filename: "
                + Arrays.stream(filenames).iterator().next().filename);
        return dao.addFileNames(filenames);
    }

    /**
     * Update one or more existing filenames
     */
    public Completable updateFileNames(DsFileName... fileNames) {
        return dao.updateFileNames(fileNames);
    }

    /**
     * Add one or more Connections for files to filenames to decentralized storage meta database.
     */
    @Override
    public Completable addFileCrossRefFileNames(DsFileCrossRefDsFileName... dsFileCrossRefDsFileNames) {
        return dao.addFileCrossRefFileNames(dsFileCrossRefDsFileNames);
    }

    @Override
    public Single<List<DsFileCrossRefDsFileName>> getCrossRefs() {
        return dao.getCrossRefs();
    }

    /**
     * Returns a list of cid's from cross reference table
     *
     * @param fileNameId the id for the filename
     * @return list of Strings of matching cid's
     */
    @Override
    public Single<List<String>> getCids(int fileNameId) {
        return dao.getCids(fileNameId);
    }

    /**
     * Returns a single filename if exists in database
     * @param fileName the name of file you are searching for
     * @param packageName the name of the calling application
     * @return DsFileName if exists otherwise null
     */
    @Override
    public Single<List<DsFileName>> getSingleFileName(String packageName, String fileName) {
        return dao.getSingleFileName(packageName, fileName);
    }

    /**
     * Returns a list of Files for a given set of cid's
     *
     * @param cids list of cids to search for
     * @return a list of files
     */
    @Override
    public Single<List<DsFile>> getFiles(List<String> cids) {
        return dao.getFiles(cids);
    }

    /**
     * Returns a single file if exists in database
     * @param cid the content crypto string for specific file
     * @return DsFile if exists otherwise null
     */
    @Override
    public Single<List<DsFile>> getSingleFile(String cid) {
        return dao.getSingleFile(cid);
    }

    /**
     * Searches the decentralized storage *metadata* by filename for files of a related CID.
     * This should be up-to-date if a sync has happened recently.
     * @param fileName file name of the file to search for.
     */
    @Override
     public Flowable<List<DsFileNamesWithFiles>> searchFilesbyName(String packageName, String fileName, int limit) {
        return dao.searchFilesbyName(packageName, fileName, limit);
    }

    /**
     * Returns the filenames for a given cid
     *
     * @param cid the cid of the file
     * @return list of rowid's for given filenames
     */
    public Flowable<List<DsFileName>> listFileNamesForCid(String cid) {
        return dao.listFileNamesForCid(cid);
    }

    /**
     * Lists the most recent files added to the decentralized storage.
     * @param limit Maximum number of files to return.
     */
    @Override
    public Flowable<List<DsFileName>> listRecentFiles(String packageName, int limit) {
        return dao.listRecentFiles(packageName, limit);
    }
}
