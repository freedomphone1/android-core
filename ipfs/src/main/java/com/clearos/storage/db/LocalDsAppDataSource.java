package com.clearos.storage.db;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

public class LocalDsAppDataSource implements DsAppDataSource {
    private DsDao dao;

    public LocalDsAppDataSource(DsDao dao) { this.dao = dao; }

    /**
     * Register one or more apps for decentralized storage access.
     */
    @Override
    public Completable registerApp(DsApp... apps) {
        return dao.registerApp(apps);
    }

    /**
     * Returns the database entry for the given package.
     */
    @Override
    public Single<DsApp> getSingleApp(String packageName) {
        return dao.getSingleApp(packageName);
    }

    /**
     * Lists all the apps that have registered with ClearCLOUD.
     */
    @Override
    public Flowable<List<DsApp>> getRegisteredApps() {
        return dao.getRegisteredApps();
    }

    /**
     * Sums the size of all files that belong to a package.
     */
    @Override
    public Single<Long> getPackageFileSize(String packageName) {
        return dao.getPackageFileSize(packageName);
    }
}
