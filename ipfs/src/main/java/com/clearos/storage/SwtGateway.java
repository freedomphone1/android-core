package com.clearos.storage;

import android.util.Log;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.clearos.dlt.DerivedKeyClient;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;

public class SwtGateway {
    public Gateway gateway = null;
    private static File metadataDir = null;
    private static final Object syncShutdown = new Object();
    private final static String TAG = "SwtGateway";

    public SwtGateway(DerivedKeyClient keyClient) throws Exception {
        gateway = new Gateway(keyClient);

        if (metadataDir == null) {
            File dataDir = keyClient.getAppContext().getFilesDir().getAbsoluteFile();
            metadataDir = new File(dataDir, "clearshare");
            if (!metadataDir.exists()) {
                if (!metadataDir.mkdir()) { throw new IOException("Couldn't create metadata directory."); }
            }
        }

        // This start method doesn't do anything if the gateway isn't already started. If it has
        // stopped, however, then it will be restarted.
        gateway.start(metadataDir.getAbsolutePath());
    }

    public Gateway getInstance() {
        return gateway;
    }

    /**
     * Shuts down the gateway client gracefully if it isn't already shutting down.
     */
    public void shutdown() {
        synchronized (syncShutdown) {
            if (gateway != null) {
                try {
                    stop();
                } catch (Exception e) {
                    Log.w(TAG, "Error shutting down decentralized storage gateway.");
                }
            }
        }
    }


    public void stop() throws Exception {
        gateway.stop();
        gateway = null;
    }
}
