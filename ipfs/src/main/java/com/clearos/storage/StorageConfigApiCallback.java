package com.clearos.storage;

import android.util.Log;

import com.clearos.dlt.ApiCallback;
import com.google.gson.Gson;

import java.util.function.Function;

import okhttp3.Call;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class StorageConfigApiCallback implements ApiCallback {
    private Function<StorageAuth, Void> callback;

    public StorageConfigApiCallback(Function<StorageAuth, Void> _callback) {
        callback = _callback;
    }

    @Override
    public void onResponse(String source, Response response) {
        Gson gson = new Gson();
        ResponseBody body = response.body();
        if (body == null) {
            callback.apply((null));
            return;
        }

        try {
            String rawResponse = body.string();
            StorageAuth result = gson.fromJson(rawResponse, StorageAuth.class);
            result.setRawResponse(rawResponse);
            callback.apply(result);
        } catch (Exception e) {
            callback.apply(null);
        }
    }

    @Override
    public void onFailure(Call call, Exception e, Response response) {
        Log.e("STORAGE-CONFIG", "Error retrieving storage config from home server.", e);
        callback.apply(null);
    }
}
