package com.clearos.storage.provider;

import java.io.File;
import java.io.IOException;

public interface IApplicationStoreHandler {
    /**
     * Event that fires when file has been successfully uploaded.
     */
    void onFileSaved(File localFile);
    /**
     * Event that fires when file error occurs during transfer.
     */
    void onIOError(IOException e);
    /**
     * Event that fires when file has been successfully downloaded.
     */
    void onFileLoaded(File localFile);
}
