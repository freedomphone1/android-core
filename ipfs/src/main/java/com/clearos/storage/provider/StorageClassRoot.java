package com.clearos.storage.provider;

import android.util.Log;

import com.clearos.dlt.DerivedKeyClient;
import com.clearos.dlt.DidKeys;
import com.clearos.storage.Gateway;
import com.clearos.storage.StorageClass;
import com.clearos.storage.StorageGeolocation;
import com.clearos.storage.StorageSpace;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Represents one of the available storage classes in ClearCLOUD.
 */
public class StorageClassRoot {
    private StorageClass storageClass;
    private StorageSpace storageSpace;
    private StorageGeolocation storageGeo;
    private static final long GB = 1024^3;
    private static final long TB = 1024^4;
    private static final String TAG = "StorClassRoot";
    private long freeSpace = 0;
    private DidKeys entity;

    public StorageClassRoot(DerivedKeyClient keyClient) {
        this.entity = keyClient.getAppKeys();
        CountDownLatch configLatch = new CountDownLatch(1);
        Gateway.getConfig(keyClient, sa -> {
            if (sa != null) {
                storageClass = sa.getLimitations().getStorageClass();
                freeSpace = Math.round(sa.getLimitations().getSize() * TB);
                storageSpace = sa.getLimitations().getStorageSpace();
                storageGeo = sa.getLimitations().getGeolocation();
            } else {
                Log.w(TAG, "Could not find configuration for decentralized storage on home server.");
            }
            configLatch.countDown();
            return null;
        });
        try {
            configLatch.await(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Log.v(TAG, "Thread interrupted while waiting for storage config API call.", e);
        }
    }

    /**
     * Returns the root key in the path of every file entry, which is the DID of the entity.
     */
    public String getRootKey() {
        return entity.getDid();
    }

    /**
     * Returns the number of available bytes on the current storage plan.
     */
    public long getFreeSpace() {
        if (freeSpace > 0) {
            return freeSpace;
        } else if (storageClass == null)
            return 0;
        else {
            long result = 0L;
            switch (storageClass) {
                case BASIC:
                    result = 5*GB;
                    break;
                case FAST:
                    result = 15*GB;
                    break;
                case PREMIUM:
                    result = TB;
                    break;
            }

            return result;
        }
    }
}
