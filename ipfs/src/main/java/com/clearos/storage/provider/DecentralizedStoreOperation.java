package com.clearos.storage.provider;

public enum DecentralizedStoreOperation {
    PUT,
    GET,
    DEL
}
