package com.clearos.storage.provider;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.clearos.dstorage.BlockHashFile;
import com.clearos.dstorage.StorageReporter;
import com.clearos.storage.meta.AsyncDao;
import com.clearos.storage.db.DbQueue;

import java.util.Date;
import java.util.Map;
import java.util.Set;

public class DbStorageReporter implements StorageReporter {
    private final DbQueue dbFile;
    private final static String TAG = "DbStorageReporter";
    private final Context context;

    DbStorageReporter(Context context, DbQueue dbFile) {
        this.context = context;
        this.dbFile = dbFile;
    }

    @Override
    public void onAnalyzed(int blockCount) {
        Log.d(TAG, "onAnalyzed called for block with index " + blockCount);
        dbFile.blocks = blockCount;
        // We don't save this right now because the operation will terminate in one of the event
        // methods below.
    }

    @Override
    public void onCancelled(Set<String> completed) {
        Log.d(TAG, "OnCancelled fired during operation.");
        dbFile.cancelled = new Date();
        dbFile.progress = completed.size();
        AsyncDao.updateQueueFiles(context,null, dbFile);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onContentErrors(Map<String, Exception> errors) {
        Log.d(TAG, String.format("%d onContentErrors fired during operation.", errors.size()));
        StringBuilder errorString = new StringBuilder();
        errorString.append(String.format("%d Content Errors: ", errors.size()));
        for (Map.Entry<String, Exception> entry: errors.entrySet()) {
            Exception e = entry.getValue();
            errorString.append(e.getMessage());
        }

        dbFile.errorMsg = errorString.toString();
        dbFile.finished = new Date();
        AsyncDao.updateQueueFiles(context,null, dbFile);
    }

    @Override
    public void onFailure(Throwable error, Set<String> completed) {
        Log.e(TAG, "onFailure fired during operation:", error);
        dbFile.progress = completed.size();
        dbFile.finished = new Date();
        dbFile.errorMsg = "Failed: " + error.getMessage();
        AsyncDao.updateQueueFiles(context,null, dbFile);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onLoadErrors(Map<String, Throwable> errors) {
        Log.d(TAG, String.format("%d onLoadErrors fired during operation.", errors.size()));
        StringBuilder errorString = new StringBuilder();
        errorString.append(String.format("%d Load Errors: ", errors.size()));
        for (Map.Entry<String, Throwable> entry: errors.entrySet()) {
            Throwable e = entry.getValue();
            errorString.append(e.getMessage());
        }

        dbFile.errorMsg = errorString.toString();
        dbFile.finished = new Date();
        AsyncDao.updateQueueFiles(context,null, dbFile);
    }

    @Override
    public void onSuccess(BlockHashFile file, Set<String> completed) {
        Log.d(TAG, String.format("%d onSuccess fired during operation for %s", completed.size(), file.getCid()));
        dbFile.finished = new Date();
        dbFile.progress = file.dag.length;
        AsyncDao.updateQueueFiles(context, null, dbFile);
    }
}
