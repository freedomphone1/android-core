package com.clearos.storage.meta;

import android.os.Handler;
import android.os.Looper;

import io.reactivex.disposables.Disposable;

public class Disposer {
    /**
     * Calls `dispose` on `d` after a delay if it isn't already disposed.
     * @param delay Number of milliseconds to wait before disposing.
     */
    public static void delayedDispose(Disposable d, long delay) {
        new Handler(Looper.getMainLooper()).postDelayed(() -> {
            if (!d.isDisposed())
                d.dispose();
        }, delay);
    }
}
