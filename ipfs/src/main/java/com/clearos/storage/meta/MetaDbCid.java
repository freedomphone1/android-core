package com.clearos.storage.meta;

public class MetaDbCid {
    private String cid;
    private long timestamp;

    public MetaDbCid(String cid, long timestamp) {
        this.cid = cid;
        this.timestamp = timestamp;
    }

    public String getCid() {
        return cid;
    }

    public long getTimestamp() {
        return timestamp;
    }
}
