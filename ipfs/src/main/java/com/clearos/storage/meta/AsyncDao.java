package com.clearos.storage.meta;

import android.content.Context;
import android.util.Log;

import androidx.sqlite.db.SimpleSQLiteQuery;

import com.clearos.storage.db.DbQueue;
import com.clearos.storage.db.DeleteAuditLog;
import com.clearos.storage.db.DsApp;
import com.clearos.storage.db.DsFile;
import com.clearos.storage.db.DsFileCrossRefDsFileName;
import com.clearos.storage.db.DsFileDeleteLogs;
import com.clearos.storage.db.DsFileExistLogs;
import com.clearos.storage.db.DsFileGetLogs;
import com.clearos.storage.db.DsFileListLogs;
import com.clearos.storage.db.DsFileName;
import com.clearos.storage.db.DsFileNamesWithFiles;
import com.clearos.storage.db.DsFilePutLogs;
import com.clearos.storage.db.DsFileWithNames;
import com.clearos.storage.db.ExistAuditLog;
import com.clearos.storage.db.GetAuditLog;
import com.clearos.storage.db.Injection;
import com.clearos.storage.db.ListAuditLog;
import com.clearos.storage.db.MetaDatabase;
import com.clearos.storage.db.PutAuditLog;
import com.clearos.storage.provider.DecentralizedStoreOperation;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import io.reactivex.CompletableObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class AsyncDao {
    private static final String TAG = "AsyncDao";

    public static void genericQuery(Context context, Function<Integer, Void> callback,
                                    SimpleSQLiteQuery query) {
        MetaDatabase database = MetaDatabase.getInstance(context);
        Disposable d = database.dao().genericQuery(query)
                .subscribeOn(Schedulers.io())
                .delay(500, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    Log.d(TAG, "SQLite query result is : " + result);
                    if (callback != null)
                        callback.apply(result);
                }, throwable -> {
                    Log.e(TAG, "On generic SQLite query", throwable);
                    if (callback != null)
                        callback.apply(null);
                });
        Disposer.delayedDispose(d, 10000);
    }

    public static void checkpoint(Context context, Function<Integer, Void> callback) {
        genericQuery(context, callback, new SimpleSQLiteQuery("pragma wal_checkpoint(full)"));
    }

    public static void deleteQueueFiles(Context context, Function<Void, Void> callback, DbQueue... queueFiles) {

    }

    public static void addFiles(Context context, Function<Boolean, Void> callback, DsFile... files) {
        Injection.provideDsFileDataSource(context).addFiles().subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
            }

            @Override
            public void onComplete() {
                Log.d(TAG, String.format("File %s was saved successfully to local db.", Arrays.toString(files)));
                if (callback != null)
                    callback.apply(true);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.e(TAG, "Error saving files to local database.", e);
                if (callback != null)
                    callback.apply(false);
            }
        });
    }

    public static void addFileNames(Context context, Function<Boolean, Void> callback, DsFileName... filenames) {
        Injection.provideDsFileDataSource(context).addFileNames(filenames).subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(@NotNull Disposable d) {
            }

            @Override
            public void onComplete() {
                Log.d(TAG, String.format("Filename %s was saved successfully to local db.", Arrays.toString(filenames)));
                if (callback != null)
                    callback.apply(true);
            }

            @Override
            public void onError(@NotNull Throwable e) {
                Log.e(TAG, "Error saving filenames to local database.", e);
                if (callback != null)
                    callback.apply(false);
            }
        });
    }

    public static void updateFileNames(Context context, Function<Boolean, Void> callback, DsFileName... filenames) {
        Injection.provideDsFileDataSource(context).updateFileNames(filenames).subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(@NotNull Disposable d) {
            }

            @Override
            public void onComplete() {
                Log.d(TAG, String.format("Filename %s was updated successfully to local db.", Arrays.toString(filenames)));
                if (callback != null)
                    callback.apply(true);
            }

            @Override
            public void onError(@NotNull Throwable e) {
                Log.e(TAG, "Error updating filenames to local database.", e);
                if (callback != null)
                    callback.apply(false);
            }
        });
    }

    public static void addFileCrossRefFileNames(
            Context context, Function<Boolean, Void> callback,
            DsFileCrossRefDsFileName... fileCrossRefFileNames) {
        Injection.provideDsFileDataSource(context).addFileCrossRefFileNames(fileCrossRefFileNames)
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(@NotNull Disposable d) {
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "Reference between cid and filename created of fileNameId "
                                + Arrays.stream(fileCrossRefFileNames).iterator().next().rowid
                                + " and cid of "
                                + Arrays.stream(fileCrossRefFileNames).iterator().next().cid);
                        if (callback != null)
                            callback.apply(true);
                    }

                    @Override
                    public void onError(@NotNull Throwable e) {
                        Log.e(TAG, "Error creating reference link");
                        if (callback != null)
                            callback.apply(false);
                    }
                });
    }

    public static void addGetLogs(Context context, Function<Boolean, Void> callback, GetAuditLog... getLogs) {
        Injection.provideDsLogDataSource(context).addGetLogs(getLogs).subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
            }

            @Override
            public void onComplete() {
                Log.d(TAG, String.format("Get logs %s was saved successfully to local db.", Arrays.toString(getLogs)));
                if (callback != null)
                    callback.apply(true);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.e(TAG, "Error saving get logs to local database.", e);
                if (callback != null)
                    callback.apply(false);
            }
        });
    }

    public static void addPutLogs(Context context, Function<Boolean, Void> callback, PutAuditLog... putLogs) {
        Injection.provideDsLogDataSource(context).addPutLogs(putLogs).subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
            }

            @Override
            public void onComplete() {
                Log.d(TAG, String.format("Put logs %s was saved successfully to local db.", Arrays.toString(putLogs)));
                if (callback != null)
                    callback.apply(true);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.e(TAG, "Error saving put logs to local database.", e);
                if (callback != null)
                    callback.apply(false);
            }
        });
    }

    public static void addExistLogs(Context context, Function<Boolean, Void> callback, ExistAuditLog... existLogs) {
        Injection.provideDsLogDataSource(context).addExistLogs(existLogs).subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
            }

            @Override
            public void onComplete() {
                Log.d(TAG, String.format("Exist logs %s was saved successfully to local db.", Arrays.toString(existLogs)));
                if (callback != null)
                    callback.apply(true);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.e(TAG, "Error saving exist logs to local database.", e);
                if (callback != null)
                    callback.apply(false);
            }
        });
    }

    public static void addListLogs(Context context, Function<Boolean, Void> callback, ListAuditLog... listLogs) {
        Injection.provideDsLogDataSource(context).addListLogs(listLogs).subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
            }

            @Override
            public void onComplete() {
                Log.d(TAG, String.format("List logs %s was saved successfully to local db.", Arrays.toString(listLogs)));
                if (callback != null)
                    callback.apply(true);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.e(TAG, "Error saving list logs to local database.", e);
                if (callback != null)
                    callback.apply(false);
            }
        });
    }

    public static void addDeleteLogs(Context context, Function<Boolean, Void> callback, DeleteAuditLog... deleteLogs) {
        Injection.provideDsLogDataSource(context).addDeleteLogs(deleteLogs).subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
            }

            @Override
            public void onComplete() {
                Log.d(TAG, String.format("Delete logs %s was saved successfully to local db.", Arrays.toString(deleteLogs)));
                if (callback != null)
                    callback.apply(true);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.e(TAG, "Error saving delete logs to local database.", e);
                if (callback != null)
                    callback.apply(false);
            }
        });
    }

    public static void addQueueFiles(Context context, Function<Boolean, Void> callback, DbQueue... queueFiles) {
        Injection.provideDbQueueDataSource(context).addQueueFiles(queueFiles).subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
            }

            @Override
            public void onComplete() {
                Log.d(TAG, String.format("Queue files for ADD %s were saved successfully to local db.", Arrays.toString(queueFiles)));
                if (callback != null)
                    callback.apply(true);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.e(TAG, "Error saving queue files for ADD to local database.", e);
                if (callback != null)
                    callback.apply(false);
            }
        });
    }

    public static void updateQueueFiles(Context context, Function<Boolean, Void> callback, DbQueue... queueFiles) {
        Injection.provideDbQueueDataSource(context).updateQueueFiles(queueFiles).subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
            }

            @Override
            public void onComplete() {
                Log.d(TAG, String.format("Queue files for UPDATE %s were saved successfully to local db.", Arrays.toString(queueFiles)));
                if (callback != null)
                    callback.apply(true);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.e(TAG, "Error saving queue files for UPDATE to local database.", e);
                if (callback != null)
                    callback.apply(false);
            }
        });
    }

    public static void registerApp(Context context, Function<Boolean, Void> callback, DsApp... apps) {
        Injection.provideDsAppDataSource(context).registerApp(apps).subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(@NonNull Disposable d) { }

            @Override
            public void onComplete() {
                Log.d(TAG, String.format("Apps %s were registered successfully to local db.", Arrays.toString(apps)));
                if (callback != null)
                    callback.apply(true);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.e(TAG, "Error registering apps to local database.", e);
                if (callback != null)
                    callback.apply(false);
            }
        });
    }

    /**
     * Returns a single filename if exists in database
     *
     * @param fileName    the name of file you are searching for
     * @param packageName the name of the calling application
     * @return DsFileName in callback if exists otherwise null
     */
    public static void getSingleFileName(Context context, String packageName,
                                         String fileName, Function<DsFileName, Void> callback) {
        Disposable d = Injection.provideDsFileDataSource(context)
                .getSingleFileName(packageName, fileName)
                .subscribeOn(Schedulers.io())
                .subscribe(result -> {
                    Log.d(TAG, "Found filename with reference " + fileName
                            + " that is " + result.get(0).filename);
                    if (callback != null)
                        if (result.toArray(new DsFileName[0]).length > 0) {
                            Log.d(TAG, "Found filename with reference " + fileName
                                    + " that is " + result.get(0).filename);
                            callback.apply(result.get(0));
                        } else {
                            callback.apply(null);
                        }
                }, throwable -> {
                    Log.e(TAG, "While searching filenames in database.", throwable);
                    if (callback != null)
                        callback.apply(null);
                });
        Disposer.delayedDispose(d, 2000);
    }

    /**
     * Returns a list of cid's from cross reference table
     *
     * @param fileNameId the id for the filename
     * @return list of Strings of matching cid's
     */
    public static void getCids(Context context, int fileNameId, Function<List<String>, Void> callback) {
        Disposable d = Injection.provideDsFileDataSource(context)
                .getCids(fileNameId)
                .subscribeOn(Schedulers.io())
                .subscribe(result -> {
                    Log.d(TAG, "Found cid references of "
                            + Arrays.toString(result.toArray(new String[0])));
                    if (callback != null)
                        callback.apply(result);
                }, throwable -> {
                    Log.e(TAG, "While searching files and aliases in database.", throwable);
                    if (callback != null)
                        callback.apply(null);
                });
        Disposer.delayedDispose(d, 2000);
    }

    /**
     * Returns a single file if exists in database
     * @param cid the content crypto string for specific file
     * @return DsFile if exists otherwise null
     */
    public static void getSingleFile(Context context, String cid, Function<DsFile, Void> callback) {
        Disposable d = Injection.provideDsFileDataSource(context)
                .getSingleFile(cid)
                .subscribeOn(Schedulers.io())
                .subscribe(result -> {
                    if (callback != null) {
                        if (result.toArray(new DsFile[0]).length > 0) {
                            Log.d(TAG, "Found file with cid reference " + cid
                                    + " that is " + result.get(0).cid);
                            callback.apply(result.get(0));
                        } else {
                            callback.apply(null);
                        }
                    }
                }, throwable -> {
                    Log.e(TAG, "While searching files in database.", throwable);
                    if (callback != null)
                        callback.apply(null);
                });
        Disposer.delayedDispose(d, 2000);
    }

    /**
     * Returns a list of Files for a given set of cid's
     *
     * @param cids list of cids to search for
     * @return a list of files
     */
    public static void getFiles(Context context, Function<DsFile[], Void> callback, List<String> cids) {
        Disposable d = Injection.provideDsFileDataSource(context).getFiles(cids)
                .subscribeOn(Schedulers.io())
                .subscribe(result -> {
                    Log.d(TAG, "Got files of: " + Arrays.toString(result.toArray(new DsFile[0])));
                    if (callback != null) {
                        callback.apply(result.toArray(new DsFile[0]));
                    }
                }, throwable -> {
                    Log.e(TAG, "While getting files from database.", throwable);
                    if (callback != null) {
                        callback.apply(null);
                    }
                });
        Disposer.delayedDispose(d, 2000);
    }

    /**
     * Returns the database entry for the given package.
     */
    public static void getSingleApp(Context context, String packageName, Function<DsApp, Void> callback) {
        Disposable d = Injection.provideDsAppDataSource(context).getSingleApp(packageName)
                .subscribeOn(Schedulers.io())
                .subscribe(result -> {
                    if (callback != null)
                        callback.apply(result);
                }, throwable -> {
                    Log.e(TAG, "While getting single app from database.", throwable);
                    if (callback != null)
                        callback.apply(null);
                });
        Disposer.delayedDispose(d, 2000);
    }

    /**
     * Lists all the apps that have registered with ClearCLOUD.
     */
    public static void getRegisteredApps(Context context, Function<DsApp[], Void> callback) {
        Disposable d = Injection.provideDsAppDataSource(context).getRegisteredApps()
                .subscribeOn(Schedulers.io())
                .subscribe(result -> {
                    if (callback != null)
                        callback.apply(result.toArray(new DsApp[0]));
                }, throwable -> {
                    Log.e(TAG, "While getting registered apps from database.", throwable);
                    if (callback != null)
                        callback.apply(null);
                });
        Disposer.delayedDispose(d, 2000);
    }

    /**
     * Sums the size of all files that belong to a package.
     */
    public static void getPackageFileSize(Context context, String packageName, Function<Long, Void> callback) {
        Disposable d = Injection.provideDsAppDataSource(context).getPackageFileSize(packageName)
                .subscribeOn(Schedulers.io())
                .subscribe(result -> {
                    if (callback != null)
                        callback.apply(result);
                }, throwable -> {
                    Log.e(TAG, "While getting registered apps from database.", throwable);
                    if (callback != null)
                        callback.apply(null);
                });
        Disposer.delayedDispose(d, 2000);
    }

    /**
     * Gets a single queue file from the database using its primary key fields.
     */
    public static void getQueueFiles(Context context, DecentralizedStoreOperation op, Function<DbQueue[], Void> callback, String... filepaths) {
        Disposable d = Injection.provideDbQueueDataSource(context).getQueueFiles(op, filepaths)
                .subscribeOn(Schedulers.io())
                .subscribe(result -> {
                    if (callback != null)
                        callback.apply(result.toArray(new DbQueue[0]));
                }, throwable -> {
                    Log.e(TAG, "While getting DbQueue from database.", throwable);
                    if (callback != null)
                        callback.apply(null);
                });
        Disposer.delayedDispose(d, 2000);
    }

    /**
     * Lists pending file operations that are queued in the database, but haven't been executed or
     * completed by the decentralized storage provider.
     *
     * @param op    Operation that was queued; one of `put`, `get`, `del`.
     * @param limit Maximum number of pending file operations to return.
     */
    public static void pendingFiles(Context context, DecentralizedStoreOperation op, int limit, Function<DbQueue[], Void> callback) {
        Disposable d = Injection.provideDbQueueDataSource(context).pendingFiles(op, limit)
                .subscribeOn(Schedulers.io())
                .subscribe(result -> {
                    if (callback != null)
                        callback.apply(result.toArray(new DbQueue[0]));
                }, throwable -> {
                    Log.e(TAG, "While getting pending DbQueue from database.", throwable);
                    if (callback != null)
                        callback.apply(null);
                });
        Disposer.delayedDispose(d, 2000);
    }

    /**
     * Lists failed file operations from the database; these were started but failed or were interrupted.
     *
     * @param op    Operation that was queued; one of `put`, `get`, `del`.
     * @param limit Maximum number of failed file operations to return.
     */
    public static void failedFiles(Context context, DecentralizedStoreOperation op, int limit, int maxRetries, Function<DbQueue[], Void> callback) {
        Disposable d = Injection.provideDbQueueDataSource(context).failedFiles(op, limit, maxRetries)
                .subscribeOn(Schedulers.io())
                .subscribe(result -> {
                    if (callback != null)
                        callback.apply(result.toArray(new DbQueue[0]));
                }, throwable -> {
                    Log.e(TAG, "While getting failed DbQueue from database.", throwable);
                    if (callback != null)
                        callback.apply(null);
                });
        Disposer.delayedDispose(d, 2000);
    }

    /**
     * Lists completed file operations that were queued in the database.
     *
     * @param op    Operation that was queued; one of `put`, `get`, `del`.
     * @param limit Maximum number of completed file operations to return.
     */
    public static void finishedFiles(Context context, DecentralizedStoreOperation op, int limit, Function<DbQueue[], Void> callback) {
        Disposable d = Injection.provideDbQueueDataSource(context).finishedFiles(op, limit)
                .subscribeOn(Schedulers.io())
                .subscribe(result -> {
                    if (callback != null)
                        callback.apply(result.toArray(new DbQueue[0]));
                }, throwable -> {
                    Log.e(TAG, "While getting finished DbQueue from database.", throwable);
                    if (callback != null)
                        callback.apply(null);
                });
        Disposer.delayedDispose(d, 2000);
    }

    /**
     * Lists running file operations that were queued in the database.
     *
     * @param op    Operation that was queued; one of `put`, `get`, `del`.
     * @param limit Maximum number of running file operations to return.
     */
    public static void runningFiles(Context context, DecentralizedStoreOperation op, int limit, Function<DbQueue[], Void> callback) {
        Disposable d = Injection.provideDbQueueDataSource(context).runningFiles(op, limit)
                .subscribeOn(Schedulers.io())
                .subscribe(result -> {
                    if (callback != null)
                        callback.apply(result.toArray(new DbQueue[0]));
                }, throwable -> {
                    Log.e(TAG, "While getting running DbQueue from database.", throwable);
                    if (callback != null)
                        callback.apply(null);
                });
        Disposer.delayedDispose(d, 2000);
    }

    /**
     * Searches the decentralized storage *metadata* by filename for files of a related CID.
     * This should be up-to-date if a sync has happened recently.
     *
     * @param fileName file name of the file to search for.
     */
    public static void searchFilesbyName(Context context, String packageName,
                                         String fileName, int limit,
                                         Function<DsFileNamesWithFiles[], Void> callback) {
        Disposable d = Injection.provideDsFileDataSource(context)
                .searchFilesbyName(packageName, fileName, limit)
                .subscribeOn(Schedulers.io())
                .subscribe(result -> {
                    if (callback != null)
                        callback.apply(result.toArray(new DsFileNamesWithFiles[0]));
                }, throwable -> {
                    Log.e(TAG, "While getting a filename from the database.", throwable);
                    if (callback != null)
                        callback.apply(null);
                });
        Disposer.delayedDispose(d, 2000);
    }

    public static void getCrossRefs(Context context) {
        Disposable d = Injection.provideDsFileDataSource(context).getCrossRefs()
                .subscribeOn(Schedulers.io())
                .subscribe(
                result -> {
                    Log.d(TAG, "The Current Cross References are: " + result.get(0).toString());
                }, throwable -> {
                    Log.e(TAG, "While getting Cross References from the database." + throwable);
                });
        Disposer.delayedDispose(d, 2000);
    }

    public static void listFileNamesForCid(Context context, String cid, Function<List<DsFileName>, Void> callback) {
        Disposable d = Injection.provideDsFileDataSource(context).listFileNamesForCid(cid)
                .subscribeOn(Schedulers.io())
                .subscribe(result -> {
                    if (callback != null)
                        callback.apply(result);
                }, throwable -> {
                    Log.e(TAG, "While listing file aliases in database.", throwable);
                    if (callback != null)
                        callback.apply(null);
                });
        Disposer.delayedDispose(d, 2000);
    }

    /**
     * Lists the most recent files added to the decentralized storage.
     *
     * @param limit Maximum number of files to return.
     */
    public static void listRecentFiles(Context context, String packageName, int limit, Function<DsFileWithNames[], Void> callback) {
        Disposable d = Injection.provideDsFileDataSource(context).listRecentFiles(packageName, limit)
                .subscribeOn(Schedulers.io())
                .subscribe(result -> {
                    if (callback != null)
                        callback.apply(result.toArray(new DsFileWithNames[0]));
                }, throwable -> {
                    Log.e(TAG, "While searching files and aliases in database.", throwable);
                    if (callback != null)
                        callback.apply(null);
                });
        Disposer.delayedDispose(d, 2000);
    }

    /**
     * Returns a list of the most recent GET audit logs against the decentralized server. These GET
     * operations may originate on this device or another.
     *
     * @param cid   CID of the file to get logs for.
     * @param limit Maximum number of log entries to return.
     */
    public static void listGetAuditLogs(Context context, String cid, int limit, Function<DsFileGetLogs[], Void> callback) {
        Disposable d = Injection.provideDsLogDataSource(context).listGetAuditLogs(cid, limit)
                .subscribeOn(Schedulers.io())
                .subscribe(result -> {
                    if (callback != null)
                        callback.apply(result.toArray(new DsFileGetLogs[0]));
                }, throwable -> {
                    Log.e(TAG, "While retrieving GET logs from database.", throwable);
                    if (callback != null)
                        callback.apply(null);
                });
        Disposer.delayedDispose(d, 2000);
    }

    /**
     * Returns a list of the most recent PUT audit logs against the decentralized server. These PUT
     * operations may originate on this device or another.
     *
     * @param cid   CID of the file to get logs for.
     * @param limit Maximum number of log entries to return.
     */
    public static void listPutAuditLogs(Context context, String cid, int limit, Function<DsFilePutLogs[], Void> callback) {
        Disposable d = Injection.provideDsLogDataSource(context).listPutAuditLogs(cid, limit)
                .subscribeOn(Schedulers.io())
                .subscribe(result -> {
                    if (callback != null)
                        callback.apply(result.toArray(new DsFilePutLogs[0]));
                }, throwable -> {
                    Log.e(TAG, "While retrieving PUT logs from database.", throwable);
                    if (callback != null)
                        callback.apply(null);
                });
        Disposer.delayedDispose(d, 2000);
    }

    /**
     * Returns a list of the most recent EXIST audit logs against the decentralized server. These EXIST
     * operations may originate on this device or another.
     *
     * @param cid   CID of the file to get logs for.
     * @param limit Maximum number of log entries to return.
     */
    public static void listExistAuditLogs(Context context, String cid, int limit, Function<DsFileExistLogs[], Void> callback) {
        Disposable d = Injection.provideDsLogDataSource(context).listExistAuditLogs(cid, limit)
                .subscribeOn(Schedulers.io())
                .subscribe(result -> {
                    if (callback != null)
                        callback.apply(result.toArray(new DsFileExistLogs[0]));
                }, throwable -> {
                    Log.e(TAG, "While retrieving EXIST logs from database.", throwable);
                    if (callback != null)
                        callback.apply(null);
                });
        Disposer.delayedDispose(d, 2000);
    }

    /**
     * Returns a list of the most recent LIST audit logs against the decentralized server. These LIST
     * operations may originate on this device or another.
     *
     * @param cid   CID of the file to get logs for.
     * @param limit Maximum number of log entries to return.
     */
    public static void listListAuditLogs(Context context, String cid, int limit, Function<DsFileListLogs[], Void> callback) {
        Disposable d = Injection.provideDsLogDataSource(context).listListAuditLogs(cid, limit)
                .subscribeOn(Schedulers.io())
                .subscribe(result -> {
                    if (callback != null)
                        callback.apply(result.toArray(new DsFileListLogs[0]));
                }, throwable -> {
                    Log.e(TAG, "While retrieving LIST logs from database.", throwable);
                    if (callback != null)
                        callback.apply(null);
                });
        Disposer.delayedDispose(d, 2000);
    }

    /**
     * Returns a list of the most recent DELETE audit logs against the decentralized server. These DELETE
     * operations may originate on this device or another.
     *
     * @param cid   CID of the file to get logs for.
     * @param limit Maximum number of log entries to return.
     */
    public static void listDeleteAuditLogs(Context context, String cid, int limit, Function<DsFileDeleteLogs[], Void> callback) {
        Disposable d = Injection.provideDsLogDataSource(context).listDeleteAuditLogs(cid, limit)
                .subscribeOn(Schedulers.io())
                .subscribe(result -> {
                    if (callback != null)
                        callback.apply(result.toArray(new DsFileDeleteLogs[0]));
                }, throwable -> {
                    Log.e(TAG, "While retrieving DELETE logs from database.", throwable);
                    if (callback != null)
                        callback.apply(null);
                });
        Disposer.delayedDispose(d, 2000);
    }
}
