package com.clearos.storage.meta;

import android.os.AsyncTask;
import android.util.Log;

import com.clearos.dstorage.IProgressHandler;
import com.clearos.dlt.IRemoteErrorHandler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.function.Function;

public class DbRestore extends AsyncTask<Void, Float, Void> {
    private IProgressHandler progressCallback;
    private Function<File, Void> doneCallback;
    private MetaDatabaseLoader loader;
    private IRemoteErrorHandler handler;

    private final String TAG = "DbRestore";

    /**
     * Starts a restore of the metadata database for decentralized storage inside the given loader.
     * @param loader Loader whose database will be restored.
     * @param handler Error handler for remote and IO errors.
     * @param doneCallback Callback for when the backup is completed.
     */
    public DbRestore(MetaDatabaseLoader loader, IRemoteErrorHandler handler, Function<File, Void> doneCallback) {
        this.progressCallback = null;
        this.doneCallback = doneCallback;
        this.handler = handler;
        this.loader = loader;
    }

    /**
     * Starts a restore of the metadata database for decentralized storage inside the given loader.
     * @param loader Loader whose database will be restored.
     * @param handler Error handler for remote and IO errors.
     * @param doneCallback Callback for when the backup is completed.
     * @param progressCallback Progress callback as each block is written.
     */
    public DbRestore(MetaDatabaseLoader loader, IRemoteErrorHandler handler, IProgressHandler progressCallback, Function<File, Void> doneCallback) {
        this.progressCallback = progressCallback;
        this.doneCallback = doneCallback;
        this.handler = handler;
        this.loader = loader;
    }

    @Override
    protected Void doInBackground(Void... voids){
        loader.getLatestCid(dbCid -> {
            try {
                String latestCid = dbCid == null ? null : dbCid.getCid();
                Log.d(TAG, "Got latest CID from db loader as " + latestCid);

                File metaDbFile = loader.getMetaDatabasePath();
                if (metaDbFile == null || latestCid == null) {
                    // We don't have a file or a database yet to restore.
                    Log.d(TAG, "No MetaDb path to restore to...");
                    doneCallback.apply(null);
                    return null;
                }

                FileOutputStream target = new FileOutputStream(metaDbFile);
                loader.getStorage().download(latestCid, target, dbf -> {
                    Log.d(TAG, "Restore database from decentralized storage. Timestamp: " + dbCid.getTimestamp());
                    if (dbf != null)
                        doneCallback.apply(metaDbFile);
                    else {
                        doneCallback.apply(null);
                    }
                    return null;
                }, progressCallback, null);
            } catch (IOException e) {
                handler.onIOError(e);
            }
            return null;
        });

        return null;
    }

    @Override
    protected void onProgressUpdate(Float... values) {
        super.onProgressUpdate(values);
        if (progressCallback != null) {
            progressCallback.onTotalProgress(values[0]);
        }
    }
}
