package com.clearos.storage;

import com.clearos.dstorage.BlockWriter;
import com.clearos.dstorage.IndexCreator;
import com.clearos.dstorage.BlockHashEncrypter;
import com.clearos.dstorage.BlockHashFile;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.grpc.stub.StreamObserver;
import tech.storewise.grpc_storage.PutResponse;

public class SwtIndexCreator implements IndexCreator {
    private String key;
    private String bucket;
    private BlockWriter writer;
    private BlockHashFile changedFile;
    private SwtGateway gateway;

    /**
     * Uploads an index file to decentralized storage.
     * @param did DID of the user that will own the file.
     * @param encryptionContext Context used for the encryption derived key generation.
     * @param file The descriptor for the blocks of the uploaded file.
     * @param _bucket An encryption context (related to the app for which data is being uploaded).
     */
    public SwtIndexCreator(SwtGateway gateway, String did, String encryptionContext, BlockHashFile file, String _bucket, BlockWriter _writer) {
        key = String.format("%s/%s/%s", did, encryptionContext, file.cid.toBase58());
        bucket = _bucket;
        writer = _writer;
        changedFile = file;
        this.gateway = gateway;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public BlockWriter getWriter() {
        return writer;
    }

    @Override
    public void start() {
        Map<String, Object> index = new HashMap<>();
        index.put("created", new Date());
        index.put("size", changedFile.size);
        byte[] block = null;

        try {
            block = BlockHashEncrypter.toCbor(index);
        } catch (Exception e) {
            writer.onGenericError(e);
        }

        gateway.getInstance().upload(bucket, key, block, new StreamObserver<PutResponse>() {
            @Override
            public void onNext(PutResponse value) {
                writer.onIndexUpdated(value.getSize());
            }

            @Override
            public void onError(Throwable t) {
                writer.onGenericError(t);
            }

            @Override
            public void onCompleted() {}
        });
    }

}
