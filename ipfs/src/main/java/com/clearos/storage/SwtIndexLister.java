package com.clearos.storage;

import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.clearos.dstorage.AwsLister;
import com.clearos.dstorage.IndexLister;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.grpc.stub.StreamObserver;
import tech.storewise.grpc_storage.ListResponse;
import tech.storewise.grpc_storage.ObjectInfo;

public class SwtIndexLister implements IndexLister {
    private AwsLister l;

    /**
     * Lists index files for a certain prefix; used to query contents in decentralized storage.
     * @param lister That has parameters and credentials and will handle the asynchronous call result.
     */
    public SwtIndexLister(AwsLister lister) {
        l = lister;
    }

    @Override
    public AwsLister getLister() {
        return l;
    }

    @Override
    public void start() {
        l.getGateway().getInstance().listObjects(l.getBucket(), l.getPrefix(), l.getContinuationToken(), new StreamObserver<ListResponse>() {
            @Override
            public void onNext(ListResponse value) {
                List<S3ObjectSummary> result = new ArrayList<>();
                for (ObjectInfo entry: value.getObjectsList()) {
                    S3ObjectSummary single = new S3ObjectSummary();
                    single.setKey(entry.getKey());
                    if (entry.hasLastModified()) {
                        // Convert the nanoseconds from protobuf timestamp into milliseconds.
                        single.setLastModified(new Date(entry.getLastModified().getNanos()*1000000));
                    }
                    single.setSize(entry.getSize());
                    result.add(single);
                }

                // We don't have pagination implemented yet at the gateway, so we just pass null
                // for the continuation token now.
                l.onResult(result, null);
            }

            @Override
            public void onError(Throwable t) {
                l.onError(t);
            }

            @Override
            public void onCompleted() {

            }
        });
    }
}
