package com.clearos.storage;

/**
 * Basic (Free): up to 15GB total storage.
 * Data Lake ($): up to 1TB total storage.
 * Data Ocean ($$): unlimited* at ($/TB).
 */
public enum StorageSpace {
    BASIC,
    LAKE,
    OCEAN
}
